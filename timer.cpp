#include "timer.h"
#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

Timer::Timer(char* name)
{
	_name = (char*)malloc(strlen(name) + 1);
	strcpy(_name, name);
	_start = micros();
}

Timer::~Timer()
{
	if (_name != NULL)
	{
		free(_name);
	}
}

void Timer::Reset()
{
	_start = micros();
}

unsigned long Timer::elapsed()
{
	return micros() - _start;
}

void Timer::DumpSerial()
{
	Serial.print(F("Timer "));
	Serial.print(_name);
	Serial.print(F(" "));
	Serial.println(elapsed());
}
