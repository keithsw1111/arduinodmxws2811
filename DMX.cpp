#include "global.h"
#include "DMX.h"
//#include "timer.h"
#include "myethernet.h"

#if defined(USEDDP) && defined(MINIMALHEADER)
	#error Cannot define USEDDP and MINIMALHEADER ... they are mutually exclusive
	error
#endif

#ifdef USEDDP
	#if defined(RELAYUNIVERSE) && defined(UNIVERSE)
		#error USEDDP can either support RELAYUNIVERSE or UNIVERSE ... not both
		error
	#endif

	#ifdef RELAYUNIVERSE
		#if RELAYUNIVERSE==0
		#else
			#error RELAYUNIVERSE must be 0 for USEDDP
			error
		#endif
	#endif

	#ifdef UNIVERSE
		#if UNIVERSE==0
		#else
			#error UNIVERSE must be 0 for USEDDP
			error
		#endif
		#if UNIVERSES==1
		#else
			#error UNIVERSES must be 1 for USEDDP
			error
		#endif
	#endif
#endif

#ifdef USEDDP
	#define DMXHEADERSIZE 10
#elif defined(MINIMALHEADER)
	#define DMXHEADERSIZE 4
#else
	#define DMXHEADERSIZE 126
#endif

DMX::DMX()
{
	_currentPacketUniverse = 0;

	#ifdef DEBUGSERIAL
		Utility::Debug(F("Free RAM at DMX Creation "));
		Utility::Debugln(Utility::FreeRam());
	#endif

	#ifdef MINIMALHEADER
		Utility::Debugln(F("DMX Mode: MINIMALHEADER"));
	#endif
	
	#ifdef USEDDP
		Utility::Debugln(F("DMX Mode: DDP"));
	#endif

	#if defined(USEDDP) || defined(MINIMALHEADER)
	#else
		Utility::Debugln(F("DMX Mode: E131"));
	#endif
	
	#ifdef USEFULLPACKETREADIFPOSSIBLE
	_packetBuffer = NULL;
	_packetBufferSize = 0;
	_packetBufferByte = -1;
	_packetBufferUsedSize = 0;
	#endif	
}

void DMX::skipbytes(short bytes)
{
	#ifdef DEBUGSERIAL
		//Utility::Debug(F("skipping "));
		//Utility::Debug(bytes);
		//Utility::Debugln(F(" bytes"));
	#endif
	
	if (bytes > 0)
	{
		int skipped = myethernet->Skip(bytes);
	}
	//for(int i = 0; i < bytes; i++)
	//{
	//	readbyte();
	//}
}

bool DMX::checkbyte(byte b)
{
	#ifdef DEBUGSERIAL
		//Utility::Debug(F("check byte "));
		//Utility::Debug(b, HEX);
		//Utility::Debug(F(" == "));
	#endif
	
	byte bin;
	readbytes((unsigned char*)&bin, sizeof(bin));
	
	#ifdef DEBUGSERIAL
		//Utility::Debugln(bin, HEX);
	#endif
	
	return (bin == b);
}

byte DMX::readbyte()
{
	byte b;
	readbytes(&b, 1);
	return b;
}

short DMX::readword()
{
	short s;
	byte b[2];
	readbytes((unsigned char*)&b[0], sizeof(b));
	s = (((short)b[0]) << 8) + b[1];
	return s;
}

short DMX::ReadWord()
{
	short s;
	byte b[2];
	Read((unsigned char*)&b[0], sizeof(b));
	s = (((short)b[0]) << 8) + b[1];
	return s;
}

long DMX::readlong()
{
	long lw;
	byte b[4];
	readbytes((unsigned char*)&b[0], sizeof(b));
	lw = (((long)b[0]) << 24) + (((long)b[1]) << 16) + (((short)b[2]) << 8) + b[3];
	return lw;
}

bool DMX::checkword(short w)
{
	#ifdef DEBUGSERIAL
		//Utility::Debug(F("check word "));
		//Utility::Debug(w, HEX);
		//Utility::Debug(F(" == "));
	#endif
	
	short s = readword();
	
	#ifdef DEBUGSERIAL
		//Utility::Debugln(s, HEX);
	#endif
	
	return (s == w);
}

bool DMX::checklong(long l)
{
	#ifdef DEBUGSERIAL
		//Utility::Debug(F("check long "));
		//Utility::Debug(l, HEX);
		//Utility::Debug(F(" == "));
	#endif
	
	long lw = readlong();
	
	#ifdef DEBUGSERIAL
		//Utility::Debugln(lw, HEX);
	#endif
	
	return(lw == l);
}

byte DMX::Read()
{
	byte b = 0;
	#ifdef USEFULLPACKETREADIFPOSSIBLE
	if (_packetBuffer != NULL)
	{
		// if we have reached the end of the buffer
		if (_packetBufferByte >= _packetBufferUsedSize)
		{
			b = 0x00;
			#ifdef DEBUGSERIAL
				Utility::Debugln(F("Read read past end of buffer."));
			#endif
		}
		else
		{
			b = *(_packetBuffer+_packetBufferByte++);
		}
	}
	else
	#endif
	{
		if (myethernet->Read((unsigned char*)&b, 1) == -1)
		{
			b = 0x00;
			#ifdef DEBUGSERIAL
				Utility::Debugln(F("Read read past end of buffer."));
			#endif
		}
	}

	return b;
}

bool DMX::readbytes(byte* p, unsigned int size)
{
	// read it
	if (myethernet->Read((unsigned char*)p, size) != size)
	{
		memset(p, 0x00, size);
		#ifdef DEBUGSERIAL
			Utility::Debugln(F("readbytes(p, size) read past end of buffer."));
		#endif
		return false;
	}
	return true;
}

bool DMX::Read(byte* p, unsigned int size)
{
	#ifdef USEFULLPACKETREADIFPOSSIBLE
	if (_packetBuffer != NULL)
	{
		// if we have reached the end of the buffer
		if (_packetBufferByte + size - 1 >= _packetBufferUsedSize)
		{
			memset(p, 0x00, size);
			#ifdef DEBUGSERIAL
				Utility::Debugln(F("Read(p, size) read past end of buffer."));
			#endif
			return false;
		}
		else
		{
			// copy from our buffer
			memcpy(p, (_packetBuffer+_packetBufferByte), size);
			_packetBufferByte += size;
			return true;
		}
	}
	else
	#endif
	{
		return readbytes(p, size);
	}
}

bool DMX::ValidUniverse()
{
	#ifdef UNIVERSE
	if (_currentPacketUniverse >= UNIVERSE && _currentPacketUniverse < UNIVERSE + OUTPUTSTRINGS)
	{
		return true;
	}
	#endif

	#ifdef RELAYUNIVERSE
	if (_currentPacketUniverse == RELAYUNIVERSE)
	{
		return true;
	}
	#endif

	if (_currentPacketUniverse == 22222)
	{
		return true;
	}
	
	return false;
}

short DMX::Left()
{
	#ifdef USEFULLPACKETREADIFPOSSIBLE
	if (_packetBuffer != NULL)
	{
		return _packetBufferUsedSize - _packetBufferByte;
	}
	else
	#endif
	{
		return myethernet->Available();
  }
}

#ifdef USEFULLPACKETREADIFPOSSIBLE
bool DMX::LoadPacketIntoBuffer(short size)
{
	if (_packetBuffer != NULL)
	{
		if (myethernet->Read(_packetBuffer, size) == size)
		{
			_packetBufferByte = 0;
			_packetBufferUsedSize = size;
			return true;
		}
	}
	
	return false;
}
#endif

bool DMX::PreparePacket(bool skip2, int ps)
{
	// skip2 is true if we were in one of the debug modes that plays with the packets

	#ifdef DEBUGSERIAL
		//Utility::Debug(F("Looking for a packet ... "));
	#endif
	int packetSize = 0;
	
	if (skip2)
	{
		packetSize = ps;
	}
	else
	{
		packetSize = myethernet->ParsePacket();
	}

	if (packetSize > DMXHEADERSIZE)
	{
		//Timer processheader("        processheader");
		#ifdef DEBUGSERIAL
			Utility::Debug(F("DMX::PreparePacket Packet received "));
			Utility::Debug(packetSize);
			Utility::Debug(F(" Skip2 "));
			Utility::Debug(skip2);
			Utility::Debug(F(" Free RAM "));
			Utility::Debugln(Utility::FreeRam());
			//Utility::Debug(F("AFTER parsePacket ... "));
			//myethernet->Dump();
		#endif
	
		_currentPacketUniverse = 0;
		
		// validate it is artnet
		bool valid = true;

		// If we have enough RAM read the whole packet into RAM ... this should be faster on the W5200 ... not on the W5100
		#ifdef USEFULLPACKETREADIFPOSSIBLE
			if (_packetBuffer != NULL && _packetBufferSize < packetSize - DMXHEADERSIZE)
			{
				free(_packetBuffer);
				_packetBuffer = NULL;
			}
			
			if (_packetBuffer == NULL && Utility::FreeRam() - 100 > packetSize - DMXHEADERSIZE)
			{
				_packetBuffer = (byte*)malloc(packetSize - DMXHEADERSIZE);
				_packetBufferSize = packetSize - DMXHEADERSIZE;

				#ifdef DEBUGSERIAL
				if (_packetBuffer == NULL)
				{
					Utility::Debugln(F("Failed to allocate buffer to read in whole packet."));
				}
				#endif
			}

			#ifdef DEBUGSERIAL
				if (_packetBuffer == NULL)
				{
					Utility::Debug(F("DMX::PreparePacket Chose NOT to read packet into a buffer. Free RAM "));
					Utility::Debugln(Utility::FreeRam());
				}
				else
				{
					Utility::Debug(F("DMX::PreparePacket Chose to read packet into a buffer. Free RAM "));
					Utility::Debugln(Utility::FreeRam());
				}
			#endif
		#endif

		#ifdef MINIMALHEADER
			if (!skip2)
			{
				valid &= checkbyte(0xBC);
				#ifdef DETAILEDPACKETCHECKING
					#ifdef DEBUGSERIAL
						Utility::Debug(F("valid MINIMALHEADER "));
						Utility::Debugln(valid);
					#endif
				#endif
				_sequence = readbyte();
			}

			_currentPacketUniverse = readword();
		#elif defined(USEDDP)
			if (!skip2)
			{
				byte flags = readbyte();
				_sequence = readbyte() & 0x0F;
				valid &= checkbyte(0x01);
				valid &= checkbyte(0x01);
				
				// we dont support offsets so check everything is zero
				valid &= checkbyte(0x00);
				valid &= checkbyte(0x00);
				valid &= checkbyte(0x00);
				valid &= checkbyte(0x00);

				int len = readword();
				
				#ifdef DETAILEDPACKETCHECKING
					#ifdef DEBUGSERIAL
						Utility::Debug(F("valid DDP "));
						Utility::Debugln(valid);
					#endif
				#endif
			}

			// universes are not used for DDP
			_currentPacketUniverse = 0;
		#else
			if (skip2)
			{
				byte check[] = { 0x00, 0x00, 0x41, 0x53, 0x43, 0x2D, 0x45, 0x31, 0x2E, 0x31};
				byte buffer0[sizeof(check)];
				valid = readbytes(&buffer0[0], sizeof(buffer0));
				valid = valid && (memcmp(&buffer0[0], &check[0], sizeof(check)) == 0);
				#ifdef DETAILEDPACKETCHECKING
					#ifdef DEBUGSERIAL
						Utility::Debug(F("valid E131 "));
						Utility::Debugln(valid);
					#endif
				#endif
			}
			else
			{
				byte check[] = { 0x00, 0x10, 0x00, 0x00, 0x41, 0x53, 0x43, 0x2D, 0x45, 0x31, 0x2E, 0x31};
				byte buffer0[sizeof(check)];
				valid = readbytes(&buffer0[0], sizeof(buffer0));
				valid = valid && (memcmp(&buffer0[0], &check[0], sizeof(check)) == 0);
				#ifdef DETAILEDPACKETCHECKING
					#ifdef DEBUGSERIAL
						Utility::Debug(F("valid ARTNET "));
						Utility::Debugln(valid);
					#endif
				#endif
			}
		#endif

		if (valid)
		{
			#ifndef MINIMALHEADER
				#ifndef USEDDP
				#ifdef DETAILEDPACKETCHECKING
					valid &= checklong(0x37000000); // 12

					#ifdef DEBUGSERIAL
						Utility::Debug(F("valid 12 "));
						Utility::Debugln(valid);
					#endif
					
					skipbytes(2);
					
					valid &= checklong(0x00000004); // 18
					#ifdef DEBUGSERIAL
						Utility::Debug(F("valid 18 "));
						Utility::Debugln(valid);
					#endif
					skipbytes(18);
					
					valid &= checklong(0x00000002); // 40
					#ifdef DEBUGSERIAL
						Utility::Debug(F("valid 40 "));
						Utility::Debugln(valid);
					#endif
					
					skipbytes(65);
					
					valid &= checkword(0x0000); // 109
					#ifdef DEBUGSERIAL
						Utility::Debug(F("valid 109 "));
						Utility::Debugln(valid);
					#endif
					
					_sequence = readbyte();
					skipbytes(1);

					// validate the universe number - 113, 114
					_currentPacketUniverse = readword();
				#else
					skipbytes(99);
					byte buffer1[4];
					valid = readbytes(&buffer1[0], sizeof(buffer1));
					_sequence = buffer1[0];
					_currentPacketUniverse = (((short)buffer1[2]) << 8) + (short)buffer1[3];
				#endif
			#endif
			#endif
			
			Utility::Debug(F("DMX::PreparePacket Universe received "));
			Utility::Debug(_currentPacketUniverse);
			Utility::Debug(F(" Sequence "));
			Utility::Debugln(_sequence);
			
			if (valid && ValidUniverse())
			{
				#if defined(MINIMALHEADER) || defined(USEDDP)
					// skip to the right channel
					skipbytes(STARTCHANNEL - 1);
					#ifdef USEFULLPACKETREADIFPOSSIBLE
						return LoadPacketIntoBuffer(packetSize-DMXHEADERSIZE-STARTCHANNEL+1);
					#else
						return true;
					#endif
				#else
					#ifdef DETAILEDPACKETCHECKING
						skipbytes(2);
						
						valid &= checkbyte(0x02); // 117
						#ifdef DEBUGSERIAL
							Utility::Debug(F("valid 117 "));
							Utility::Debugln(valid);
						#endif
						
						valid &= checkbyte(0xA1); // 118
						#ifdef DEBUGSERIAL
							Utility::Debug(F("valid 118 "));
							Utility::Debugln(valid);
						#endif
						
						valid &= checkword(0x0000); // 119
						#ifdef DEBUGSERIAL
							Utility::Debug(F("valid 119 "));
							Utility::Debugln(valid);
						#endif
						
						valid &= checkword(0x0001); // 121
						#ifdef DEBUGSERIAL
							Utility::Debug(F("valid 121 "));
							Utility::Debugln(valid);
						#endif
						
						skipbytes(3);
					#else
						skipbytes(11);
					#endif
				
					#ifdef DETAILEDPACKETCHECKING
						if (valid)
						{
					#endif
							// skip to the right channel
							skipbytes(STARTCHANNEL - 1);
							#ifdef USEFULLPACKETREADIFPOSSIBLE
								return LoadPacketIntoBuffer(packetSize-DMXHEADERSIZE-STARTCHANNEL+1);
							#else
								return true;
							#endif

					#ifdef DETAILEDPACKETCHECKING
						}
						else
						{
							#if defined(DEBUGSERIAL) || defined(DEBUGDUMPPIN)
								Utility::Debugln(F("Packet rejected due to additional validation."));
								return false;
							#endif
						}
					#endif
				#endif
			}
			else
			{
				#if defined(DEBUGSERIAL) || defined(DEBUGDUMPPIN)
					Utility::Debug(F("Packet rejected due to additional validation or unrecognised universe "));
					Utility::Debugln(_currentPacketUniverse);
				#endif

				return false;
			}
		}

		if (!valid)
		{
			#if defined(DEBUGSERIAL) || defined(DEBUGDUMPPIN)
				Utility::Debug(F("DMX::PreparePacket Packet does not look like ARTNET "));
				Utility::Debugln(packetSize);
			#endif

			return false;
		}
	}
	else if (packetSize == 0)
	{
		#ifdef DEBUGSERIAL
			//Utility::Debugln(F("No packet found"));
		#endif
		_currentPacketUniverse = 0;
		return false;
	}
	else if (packetSize < 0)
	{
		Utility::Debugln(F("DMX::PreparePacket Negative packet size - resetting Udp."));
		
		// this should not happen so lets reset
		myethernet->Reset();
		return false;
	}
	else
	{
		Flush();
		_currentPacketUniverse = 0;
		
		#if defined(DEBUGSERIAL) || defined (DEBUGDUMPPIN)
			Utility::Debug(F("DMX::PreparePacket Packet skipped ... too small "));
			Utility::Debugln(packetSize);
		#endif

		return false;
	}
}

void DMX::Flush()
{
	#if defined(DEBUGSERIAL) || defined (DEBUGDUMPPIN)
		//Utility::Debug(F("Flushing with "));
		//Utility::Debug(myethernet->Available());
		//Utility::Debugln(F(" available bytes."));
	#endif

	#ifdef USEFULLPACKETREADIFPOSSIBLE
	// if packet buffer isnt null then i should have read the whole packet ... ergo nothing to flush
	if (_packetBuffer == NULL)
	#endif
	{
		myethernet->Flush();
	}
}
