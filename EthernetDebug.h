#ifndef ETHERNET_DEBUG
	#define ETHERNET_DEBUG
	
	extern void ethernetDumpState(byte level);
	extern void ethernetDumpAllSockets(byte level);

#endif