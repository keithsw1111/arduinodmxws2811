#ifndef RELAYMAPPER_H
	#define RELAYMAPPER_H

	#ifdef RELAYUNIVERSE

	#ifdef BATCHUPRELAYS	

		// if you use this code then all pins on these ports effectively cannot be used for output as we will be writing to them whenever we output
	
		#define PA 0
		//#define PB 1 - UNUSED PD WOULD INTERFERE WITH SPI
		#define PC 2
		#define PD 3
		#define PF 4
		#define PG 5
		#define PK 6
		#define PL 7

	        const char RelayPortMap[] PROGMEM = { PC,PC,PC,PC,
												PD,PG,PL,PL,
												PC,PC,PC,PC,
												PG,PG,PL,PL,
												PK,PK,PK,PK,
												PK,PK,PK,PK };

	        const char RelayPortPinMap[] PROGMEM = { 7,5,3,1,
        	                           7,1,7,5,
                	                   6,4,2,0,
                        	           2,0,6,4,
                                	   0,1,2,3,
	                                   4,5,6,7 };
	#endif

	const char RelayMap[] PROGMEM = { 30, 32, 34, 36,
									38, 40, 42, 44,
									31, 33, 35, 37,         
									39, 41, 43, 45,     
									62, 63, 64, 65,
									66, 67, 68, 69 };
					  
	#endif
	
#endif