#include "global.h"
#include "ethernetdebug.h"
#include "socketdebug.h"

#ifdef USEENC28J60
	#include <utility/Enc28J60Network.h>
	void ethernetDumpState(byte level)
	{
		//Serial.print(F("Revision: "));
		//Serial.print(Enc28J60Network::getrev());
		//Serial.print(F("Link Status: "));
		//Serial.print(Enc28J60Network::linkStatus());
		Serial.print(F("IP Address: "));
		uip_ipaddr_t a;
		uip_gethostaddr(a);
		Serial.print(ip_addr_uip(a));
		Serial.print(F(", Subnet: "));
		uip_getnetmask(a);
		Serial.print(ip_addr_uip(a));
		uint16_t rxstart = Enc28J60Network::readRegPair(ERXSTL);
		Serial.print(F(", RXStart(ERXSTL): 0x"));
		Serial.print(rxstart, HEX);
		uint16_t rxend = Enc28J60Network::readRegPair(ERXNDL);
		Serial.print(F(", RXEnd(ERXNDL): 0x"));
		Serial.print(rxend, HEX);
		Serial.print(F(", TXStart(ETXSTL): 0x"));
		Serial.print(Enc28J60Network::readRegPair(ETXSTL), HEX);
		Serial.print(F(", TXEnd(ETXNDL): 0x"));
		Serial.println(Enc28J60Network::readRegPair(ETXNDL), HEX);
		Serial.print(F("RXReadPtr(ERDPTL) r: 0x"));
		Serial.print(Enc28J60Network::readRegPair(ERDPTL), HEX);
		Serial.print(F(", TXWritePtr(EWRPTL) w: 0x"));
		Serial.print(Enc28J60Network::readRegPair(EWRPTL), HEX);
		uint16_t intreadptr = Enc28J60Network::readRegPair(ERXRDPTL);
		uint16_t intwriteptr = Enc28J60Network::readRegPair(ERXWRPTL);
		uint16_t free = 0;
		if (intwriteptr > intreadptr)
		{
			free = (rxend-rxstart)-(intwriteptr - intreadptr);
		}
		else if (intreadptr == intwriteptr)
		{
			free = (rxend-rxstart);
		}
		else
		{
			free = intreadptr - intwriteptr - 1;
		}
		Serial.print(F(", RXIntReadPtr(ERXRDPTL) s: 0x"));
		Serial.print(intreadptr, HEX);
		Serial.print(F(", RXIntWritePtr(ERXWRPTL) e: 0x"));
		Serial.print(intwriteptr, HEX);
		Serial.print(F(", Free ENC Memory: 0x"));
		Serial.println(free, HEX);	
		Serial.print(F("Link Status:"));
		Serial.print(Ethernet.linkStatus());
		bool rcvenabled = (Enc28J60Network::readReg(ECON1) & 0x04) > 0;
		Serial.print(F(", Receive enabled:"));
		Serial.println(rcvenabled);
	}
#else
	#include <utility/w5100.h>

	void ethernetDumpState(byte level)
	{
		SPI.beginTransaction(SPI_ETHERNET_SETTINGS);

		// W5100 & W5200 common items

		// Mode (MR) 0x0000 - lvl 0
		byte mr = W5100.readMR();
		Serial.print(F("Ethernet Mode "));
		Serial.print(mr, BIN);
		
		// Subnet Address (SUBR) 0x0005-0x0008 - lvl 0
		unsigned long subr;
		W5100.readSUBR((byte*)&subr);
		Serial.print(F(",Subnet Mask "));
		for (int i = 0; i < sizeof(subr); i++)
		{
			Serial.print(*(((byte*)&subr) + i));
			Serial.print(F("."));
		}
		
		// MAC Address (SHAR) 0x0009-0x000E - lvl 0
		byte shar[6];
		W5100.readSHAR((byte*)&shar);
		Serial.print(F(",MAC Address: "));
		for (int i = 0; i < sizeof(shar); i++)
		{
			Serial.print(*(((byte*)&shar[0]) +i), HEX);
			Serial.print(F(":"));
		}

		// IP Address (SIPR) 0x000F-0x0012 - lvl 0
		unsigned long sipr;
		W5100.readSIPR((byte*)&sipr);
		Serial.print(F(",IP Address "));
		for (int i = 0; i < sizeof(sipr); i++)
		{
			Serial.print(*(((byte*)&sipr) + i));
			Serial.print(F("."));
		}

		if (level > 0)
		{
			// Gateway Address (GAR) 0x0001-0x0004 - lvl 1
			unsigned long gar;
			W5100.readGAR((byte*)&gar);
			Serial.print(F(",Gateway "));
			for (int i = 0; i < sizeof(gar); i++)
			{
				Serial.print(*(((byte*)&gar) + i));
				Serial.print(F("."));
			}
		}
		
		if (level > 1)
		{
			// Interrupt (IR) 0x0015 - lvl 2
			byte ir = W5100.readIR();
			Serial.print(F(",Interrupt "));
			Serial.print(ir, BIN);

			// Interrupt Mask (IMR) 0x0016 - lvl 2
			byte imr = W5100.readIMR();
			Serial.print(F(",Interrupt Mask "));
			Serial.print(imr);
		}
		
		if (level > 2)
		{
			// Retry Time (RTR) 0x0017-0x0018 - lvl 3
			uint16_t rtr = W5100.readRTR_W5100_W5200();
			Serial.print(F(",Retry Time "));
			Serial.print(rtr);
			
			// Retry Count (RCR) 0x0019 - lvl 3
			byte rcr = W5100.readRCR_W5100_W5200();
			Serial.print(F(",Retry Count "));
			Serial.print(rcr);

			// PPP LCP Request Timer (PTIMER) 0x0028 - lvl 3
			byte ptimer = W5100.readPTIMER();
			Serial.print(F(",PPP LCP Request Timer "));
			Serial.print(ptimer);
			
			// PPP LCP Magic Number (PMAGIC) 0x0029 - lvl 3
			byte pmagic = W5100.readPMAGIC();
			Serial.print(F(",PPP LCP Magic Number "));
			Serial.print(pmagic);
		}
		
		if (level > 3)
		{
			// Auth type in PPoE (PATR) 0x001C-0x001D - lvl 4
			uint16_t patr = W5100.readPATR();
			Serial.print(F(",Auth Type "));
			Serial.print(patr);
		}
		
		// W5100 only items
		// Receive Memory Size (RMSR) 0x001A - lvl 0
		byte rmsr = W5100.readRMSR();
		Serial.print(F(",Receive Memory Size "));
		Serial.print(rmsr);
		
		// Transmit Memory Size (TMSR) 0x001B - lvl 0
		byte tmsr = W5100.readTMSR();
		Serial.print(F(",Transmit Memory Size "));
		Serial.print(tmsr);
		
		if (level > 3)
		{
			// Unreachable IP Address (UIPR) 0x002A-0x002D - lvl 4
			unsigned long uipr;
			W5100.readUIPR_W5100_W5200((byte*)&uipr);
			Serial.print(F(",Unreachable IP Address "));
			for (int i = 0; i < sizeof(uipr); i++)
			{
				Serial.print(*(((byte*)&uipr) + i));
				Serial.print(F("."));
			}
			
			// Unreachable Port (UPORT) 0x002E-0x002F - lvl 4
			uint16_t uport = W5100.readUPORT_W5100_W5200();
			Serial.print(F(",Unreachable Port "));
			Serial.print(uport);
		}
		
		// W5200 only items
		// Chip version (VERSIONR) 0x001F - lvl 0
		byte versionr = W5100.readVERSIONR();
		Serial.print(F(",Chip Version "));
		Serial.print(versionr);

		// PHY Status (PSTATUS) 0x0035 - lvl 0
		byte pstatus = W5100.readPHY();
		Serial.print(F(",PSTATUS "));
		Serial.print(pstatus, BIN);
		
		if (level > 1)
		{
			// Socket Interrupt (IR2) 0x0034 - lvl 2
			byte ir2 = W5100.readIR2();
			Serial.print(F(",Socket Interrupt "));
			Serial.print(ir2);

			// Socket Interrupt Mask (IMR2) 0x0036 - lvl 2
			byte imr2 = W5100.readIMR2();
			Serial.print(F(",Socket Interrupt Mask "));
			Serial.print(imr2);
		}
		
		if (level > 3)
		{
			// Auth algorithm in PPoE (PPPALGO) 0x001E - lvl 4
			byte pppalgo = W5100.readPPPALGO();
			Serial.print(F(",Auth Algorithm "));
			Serial.print(pppalgo);

			// Interrupt Low Level Timer (INTLEVEL) 0x0030-0x0031 - lvl 4
			uint16_t intlevel = W5100.readINTLEVEL();
			Serial.print(F(",Interrupt Low Level Timer "));
			Serial.print(intlevel);
		}
		
		Serial.println(F(""));

		SPI.endTransaction();
	}
#endif
	
void ethernetDumpAllSockets(byte level)
{
	socketDumpStateAll(level);
}