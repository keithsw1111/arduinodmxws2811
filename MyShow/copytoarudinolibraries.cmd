cd ..

rem set root="d:\Lights\Arduino\arduino-1.6.5-r2-windows\arduino-1.6.5-r2"

set root="C:\Users\keith\Documents\arduino-1.6.12"


mkdir %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
mkdir %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
mkdir %root%"\examples\DMXControllerWS2811_BALCONY"
mkdir %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
mkdir %root%"\examples\DMXControllerWS2811_TESTER"

echo y | del %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES\*.*"
echo y | del %root%"\examples\DMXControllerWS2811_HARRY_ROSES\*.*"
echo y | del %root%"\examples\DMXControllerWS2811_BALCONY\*.*"
echo y | del %root%"\examples\DMXControllerWS2811_GARAGE_HARRY\*.*"
echo y | del %root%"\examples\DMXControllerWS2811_TESTER\*.*"

copy DMXControllerWS2811.ino %root%\examples\DMXControllerWS2811_DRIVEWAY_ROSES\DMXControllerWS2811_DRIVEWAY_ROSES.ino
copy DMXControllerWS2811.ino %root%\examples\DMXControllerWS2811_HARRY_ROSES\DMXControllerWS2811_HARRY_ROSES.ino
copy DMXControllerWS2811.ino %root%\examples\DMXControllerWS2811_BALCONY\DMXControllerWS2811_BALCONY.ino
copy DMXControllerWS2811.ino %root%\examples\DMXControllerWS2811_GARAGE_HARRY\DMXControllerWS2811_GARAGE_HARRY.ino
copy DMXControllerWS2811.ino %root%\examples\DMXControllerWS2811_TESTER\DMXControllerWS2811_TESTER.ino

copy myshow\Global_DRIVEWAY_ROSES.h %root%\examples\DMXControllerWS2811_DRIVEWAY_ROSES\global.h
copy myshow\Global_HARRY_ROSES.h %root%\examples\DMXControllerWS2811_HARRY_ROSES\global.h
copy myshow\Global_BALCONY.h %root%\examples\DMXControllerWS2811_BALCONY\global.h
copy myshow\Global_GARAGE_HARRY.h %root%\examples\DMXControllerWS2811_GARAGE_HARRY\global.h
copy myshow\Global_TESTER.h %root%\examples\DMXControllerWS2811_TESTER\global.h

copy relaymapping.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy bulbtracker.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy ws2811protocol.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy fastbitmanipulation.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy display.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy DMX.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy utility.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy relays.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy myethernet.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy timer.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy flashstringhelper.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy ethernetdebug.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy socketdebug.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy debug.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy bulbmultiplier.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"
copy colourmapping.* %root%"\examples\DMXControllerWS2811_GARAGE_HARRY"

copy relaymapping.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy bulbtracker.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy ws2811protocol.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy fastbitmanipulation.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy display.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy DMX.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy utility.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy relays.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy myethernet.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy timer.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy flashstringhelper.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy ethernetdebug.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy socketdebug.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy debug.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy bulbmultiplier.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"
copy colourmapping.* %root%"\examples\DMXControllerWS2811_DRIVEWAY_ROSES"

copy relaymapping.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy bulbtracker.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy ws2811protocol.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy fastbitmanipulation.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy display.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy DMX.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy utility.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy relays.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy myethernet.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy timer.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy flashstringhelper.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy ethernetdebug.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy socketdebug.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy debug.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy bulbmultiplier.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"
copy colourmapping.* %root%"\examples\DMXControllerWS2811_HARRY_ROSES"

copy relaymapping.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy bulbtracker.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy ws2811protocol.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy fastbitmanipulation.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy display.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy DMX.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy utility.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy relays.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy myethernet.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy timer.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy flashstringhelper.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy ethernetdebug.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy socketdebug.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy debug.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy bulbmultiplier.* %root%"\examples\DMXControllerWS2811_BALCONY"
copy colourmapping.* %root%"\examples\DMXControllerWS2811_BALCONY"

copy relaymapping.* %root%"\examples\DMXControllerWS2811_TESTER"
copy bulbtracker.* %root%"\examples\DMXControllerWS2811_TESTER"
copy ws2811protocol.* %root%"\examples\DMXControllerWS2811_TESTER"
copy fastbitmanipulation.* %root%"\examples\DMXControllerWS2811_TESTER"
copy display.* %root%"\examples\DMXControllerWS2811_TESTER"
copy DMX.* %root%"\examples\DMXControllerWS2811_TESTER"
copy utility.* %root%"\examples\DMXControllerWS2811_TESTER"
copy relays.* %root%"\examples\DMXControllerWS2811_TESTER"
copy myethernet.* %root%"\examples\DMXControllerWS2811_TESTER"
copy timer.* %root%"\examples\DMXControllerWS2811_TESTER"
copy flashstringhelper.* %root%"\examples\DMXControllerWS2811_TESTER"
copy ethernetdebug.* %root%"\examples\DMXControllerWS2811_TESTER"
copy socketdebug.* %root%"\examples\DMXControllerWS2811_TESTER"
copy debug.* %root%"\examples\DMXControllerWS2811_TESTER"
copy bulbmultiplier.* %root%"\examples\DMXControllerWS2811_TESTER"
copy colourmapping.* %root%"\examples\DMXControllerWS2811_TESTER\colourmapping.h"

rem pause