#ifndef GLOBAL_H
	#define GLOBAL_H
	
	// Choose the type of ethernet chip you are using
	#define USEENC28J60
	//#define USEW5100
	//#define USEW5200
	
	// When defined the normal 125 byte header is reduced to 4 bytes ... saves network data and thus makes the
	// program faster but requires a program to strip it down
	//#define MINIMALHEADER

	// When defined the program wont listen for E1.31 but the DDP protocol instead
	//#define USEDDP
	
	#include "debug.h"
	
	// this is the default colour and relay state displayed if the controller does not receive a packet in a long time
	#define DEFAULTCOLOUR RED
	#define DEFAULTRELAYSTATE ON
	
	// if this is defined then the board will reboot if it does not receive a network packet for the specified timeout interval DEFAULTONTIME
	// If not defined then it will just delete and recreate the network connection
	// define it if once losing connections your controller does not reconnect automatically
	//#define REBOOTONPACKETTIMEOUT 1
	
	// disable start pattern which is defined in debug.h
	//#undef STARTPATTERN

	// If no packet is seen for this long then everything turns on/goes white
	#define DEFAULTONTIME 15
	
	// first universe ... also determines IP address
	// Comment this out for a Relay only controller
	#define UNIVERSE 40

	// length of longest string ... code is faster if this is shorter and less memory used
	#define MAXBULBS 96 

	// Number of strings ... code is faster if this is smaller and less memory used
	#define OUTPUTSTRINGS 1 

	// The universe our relay instructions arrive in
	#define RELAYUNIVERSE 1

	// This is the number of physical pixels that a DMX pixel actually impacts ... typically 1
	#define PHYSICALTOLOGICALRATIO 1

	// this is the IP Address of the arduino
	#define IP_BYTE_1 192
	#define IP_BYTE_2 168
	#define IP_BYTE_3 0
	#define IP_BYTE_4 230
	
	// large numbers lead to long bulb output ... shorter keeps more on top of network traffic but bulbs may not update often enough
	#define MAXPACKETSBETWEENDISPLAY 2
	
	// Maximum network packet checking loops with no bulb updates before we just refresh the bulbs for the heck of it
	// -1 disables this
	#define MAXLOOPSBETWEENBULBUPDATES -1
	
#endif