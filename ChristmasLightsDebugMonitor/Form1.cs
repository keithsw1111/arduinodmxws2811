﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;

namespace ChristmasLightsDebugMonitor
{
    public partial class Form1 : Form
    {
        static Form1 me = null;
        Thread _t = null;
        bool _stopUpdating = false;
        Dictionary<string, TextWriter> _logs = new Dictionary<string, TextWriter>();

        public Form1()
        {
            InitializeComponent();
        }

        delegate void SetTextCallback(string ip, string text);

        private void SetText(string ip, string text)
        {
            if (!_stopUpdating)
            {
                // InvokeRequired required compares the thread ID of the
                // calling thread to the thread ID of the creating thread.
                // If these threads are different, it returns true.

                if (this.textBox1.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(SetText);
                    try
                    {
                        this.Invoke(d, new object[] { ip, text });
                    }
                    catch
                    {
                    }
                }
                else
                {
                    textBox1.SuspendLayout();
                    if (textBox1.Text.Length > 30000)
                    {
                        // cut off the firsy 10,000 characters
                        textBox1.Text = textBox1.Text.Substring(10000);
                    }

                    textBox1.AppendText(text); // +"\r\n";
                    if (autoscroll.Checked)
                    {
                        if (textBox1.Text.Length > 0)
                        {
                            textBox1.Select(textBox1.Text.Length - 1, 1);
                            textBox1.ScrollToCaret();
                        }
                    }
                    textBox1.ResumeLayout();

                    if (!_logs.ContainsKey(ip))
                    {
                        _logs[ip] = new StreamWriter(ip + ".log", true);
                        _logs[ip].WriteLine("");
                        _logs[ip].WriteLine("");
                        _logs[ip].WriteLine("");
                        _logs[ip].WriteLine("**************************************************");
                        _logs[ip].WriteLine("");
                        _logs[ip].WriteLine(DateTime.Now.ToString());
                        _logs[ip].WriteLine("");
                        _logs[ip].WriteLine("**************************************************");
                    }

                    _logs[ip].Write(text);
                }
            }
        }

        static void Monitor()
        {
            UdpClient newsock = new UdpClient();
            newsock.ExclusiveAddressUse = false;
            newsock.Client.Bind(new IPEndPoint(IPAddress.Any, 22222));

            byte[] message;
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            bool lasthadCR = true;
            while (true)
            {
                if (newsock.Available > 0)
                {
                    message = newsock.Receive(ref sender);

                    if (message[0] == 0xBC)
                    {
                        // this is an ack packet ... we will ignore it
                    }
                    else
                    {
                        string s = string.Empty;
                        foreach (byte b in message)
                        {
                            s += (char)b;
                        }
                        if (lasthadCR)
                        {
                            Form1.me.SetText(sender.Address.ToString(), sender.ToString() + " : " + s);
                        }
                        else
                        {
                            Form1.me.SetText(sender.Address.ToString(), s);
                        }

                        lasthadCR = s.EndsWith("\n");
                    }
                }
                else
                {
                    Thread.Sleep(1);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            me = this;

            ThreadStart ts = new ThreadStart(Monitor);
            _t = new Thread(ts);
            _t.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _stopUpdating = true;
            if (_t != null)
            {
                _t.Abort();
                _t.Join();
                _t = null;
            }
        }
    }
}
