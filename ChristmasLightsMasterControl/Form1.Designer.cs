﻿namespace ChristmasLightsMasterControl
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.all = new System.Windows.Forms.CheckBox();
            this.controller = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.debugOff = new System.Windows.Forms.Button();
            this.debugON = new System.Windows.Forms.Button();
            this.sendMaxLoopsBetweenBulbUpdates = new System.Windows.Forms.Button();
            this.sendMaxPacketsBetweenDisplay = new System.Windows.Forms.Button();
            this.maxLoopsBetweenBulbUpdates = new System.Windows.Forms.NumericUpDown();
            this.maxPacketsBetweenDisplay = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dmx = new System.Windows.Forms.Button();
            this.debug3 = new System.Windows.Forms.Button();
            this.debug2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.debug1 = new System.Windows.Forms.Button();
            this.ResetW5x00 = new System.Windows.Forms.Button();
            this.reset = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxLoopsBetweenBulbUpdates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxPacketsBetweenDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // all
            // 
            this.all.AutoSize = true;
            this.all.Cursor = System.Windows.Forms.Cursors.Default;
            this.all.Location = new System.Drawing.Point(12, 15);
            this.all.Name = "all";
            this.all.Size = new System.Drawing.Size(37, 17);
            this.all.TabIndex = 0;
            this.all.Text = "All";
            this.all.UseVisualStyleBackColor = true;
            this.all.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // controller
            // 
            this.controller.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controller.FormattingEnabled = true;
            this.controller.Location = new System.Drawing.Point(160, 12);
            this.controller.Name = "controller";
            this.controller.Size = new System.Drawing.Size(234, 21);
            this.controller.Sorted = true;
            this.controller.TabIndex = 1;
            this.controller.SelectedIndexChanged += new System.EventHandler(this.controller_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "or Controller:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.debugOff);
            this.groupBox1.Controls.Add(this.debugON);
            this.groupBox1.Controls.Add(this.sendMaxLoopsBetweenBulbUpdates);
            this.groupBox1.Controls.Add(this.sendMaxPacketsBetweenDisplay);
            this.groupBox1.Controls.Add(this.maxLoopsBetweenBulbUpdates);
            this.groupBox1.Controls.Add(this.maxPacketsBetweenDisplay);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dmx);
            this.groupBox1.Controls.Add(this.debug3);
            this.groupBox1.Controls.Add(this.debug2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.debug1);
            this.groupBox1.Controls.Add(this.ResetW5x00);
            this.groupBox1.Controls.Add(this.reset);
            this.groupBox1.Location = new System.Drawing.Point(13, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 227);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Actions";
            // 
            // debugOff
            // 
            this.debugOff.Location = new System.Drawing.Point(109, 50);
            this.debugOff.Name = "debugOff";
            this.debugOff.Size = new System.Drawing.Size(96, 23);
            this.debugOff.TabIndex = 9;
            this.debugOff.Text = "UDP Debug OFF";
            this.debugOff.UseVisualStyleBackColor = true;
            this.debugOff.Click += new System.EventHandler(this.debugOff_Click);
            // 
            // debugON
            // 
            this.debugON.Location = new System.Drawing.Point(7, 50);
            this.debugON.Name = "debugON";
            this.debugON.Size = new System.Drawing.Size(96, 23);
            this.debugON.TabIndex = 8;
            this.debugON.Text = "UDP Debug ON";
            this.debugON.UseVisualStyleBackColor = true;
            this.debugON.Click += new System.EventHandler(this.debugON_Click);
            // 
            // sendMaxLoopsBetweenBulbUpdates
            // 
            this.sendMaxLoopsBetweenBulbUpdates.Location = new System.Drawing.Point(296, 134);
            this.sendMaxLoopsBetweenBulbUpdates.Name = "sendMaxLoopsBetweenBulbUpdates";
            this.sendMaxLoopsBetweenBulbUpdates.Size = new System.Drawing.Size(75, 23);
            this.sendMaxLoopsBetweenBulbUpdates.TabIndex = 7;
            this.sendMaxLoopsBetweenBulbUpdates.Text = "Send";
            this.sendMaxLoopsBetweenBulbUpdates.UseVisualStyleBackColor = true;
            this.sendMaxLoopsBetweenBulbUpdates.Click += new System.EventHandler(this.sendMaxLoopsBetweenBulbUpdates_Click);
            // 
            // sendMaxPacketsBetweenDisplay
            // 
            this.sendMaxPacketsBetweenDisplay.Location = new System.Drawing.Point(296, 110);
            this.sendMaxPacketsBetweenDisplay.Name = "sendMaxPacketsBetweenDisplay";
            this.sendMaxPacketsBetweenDisplay.Size = new System.Drawing.Size(75, 23);
            this.sendMaxPacketsBetweenDisplay.TabIndex = 7;
            this.sendMaxPacketsBetweenDisplay.Text = "Send";
            this.sendMaxPacketsBetweenDisplay.UseVisualStyleBackColor = true;
            this.sendMaxPacketsBetweenDisplay.Click += new System.EventHandler(this.sendMaxPacketsBetweenDisplay_Click);
            // 
            // maxLoopsBetweenBulbUpdates
            // 
            this.maxLoopsBetweenBulbUpdates.Location = new System.Drawing.Point(185, 137);
            this.maxLoopsBetweenBulbUpdates.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.maxLoopsBetweenBulbUpdates.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.maxLoopsBetweenBulbUpdates.Name = "maxLoopsBetweenBulbUpdates";
            this.maxLoopsBetweenBulbUpdates.Size = new System.Drawing.Size(104, 20);
            this.maxLoopsBetweenBulbUpdates.TabIndex = 6;
            this.maxLoopsBetweenBulbUpdates.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // maxPacketsBetweenDisplay
            // 
            this.maxPacketsBetweenDisplay.Location = new System.Drawing.Point(185, 111);
            this.maxPacketsBetweenDisplay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.maxPacketsBetweenDisplay.Name = "maxPacketsBetweenDisplay";
            this.maxPacketsBetweenDisplay.Size = new System.Drawing.Size(104, 20);
            this.maxPacketsBetweenDisplay.TabIndex = 6;
            this.maxPacketsBetweenDisplay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Max Loops Between Bulb Updates";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Max Packets Between Display";
            // 
            // dmx
            // 
            this.dmx.Location = new System.Drawing.Point(254, 81);
            this.dmx.Name = "dmx";
            this.dmx.Size = new System.Drawing.Size(75, 23);
            this.dmx.TabIndex = 4;
            this.dmx.Text = "DMX";
            this.dmx.UseVisualStyleBackColor = true;
            this.dmx.Click += new System.EventHandler(this.dmx_Click);
            // 
            // debug3
            // 
            this.debug3.Location = new System.Drawing.Point(172, 81);
            this.debug3.Name = "debug3";
            this.debug3.Size = new System.Drawing.Size(75, 23);
            this.debug3.TabIndex = 3;
            this.debug3.Text = "Debug 3";
            this.debug3.UseVisualStyleBackColor = true;
            this.debug3.Click += new System.EventHandler(this.debug3_Click);
            // 
            // debug2
            // 
            this.debug2.Location = new System.Drawing.Point(90, 81);
            this.debug2.Name = "debug2";
            this.debug2.Size = new System.Drawing.Size(75, 23);
            this.debug2.TabIndex = 2;
            this.debug2.Text = "Debug 2";
            this.debug2.UseVisualStyleBackColor = true;
            this.debug2.Click += new System.EventHandler(this.debug2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 165);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Ethernet Test 1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(102, 165);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(88, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Ethernet Test 2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(196, 165);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(88, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Ethernet Test 3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // debug1
            // 
            this.debug1.Location = new System.Drawing.Point(8, 81);
            this.debug1.Name = "debug1";
            this.debug1.Size = new System.Drawing.Size(75, 23);
            this.debug1.TabIndex = 1;
            this.debug1.Text = "Debug 1";
            this.debug1.UseVisualStyleBackColor = true;
            this.debug1.Click += new System.EventHandler(this.debug1_Click);
            // 
            // ResetW5x00
            // 
            this.ResetW5x00.Location = new System.Drawing.Point(88, 20);
            this.ResetW5x00.Name = "ResetW5x00";
            this.ResetW5x00.Size = new System.Drawing.Size(81, 23);
            this.ResetW5x00.TabIndex = 0;
            this.ResetW5x00.Text = "Reset W5x00";
            this.ResetW5x00.UseVisualStyleBackColor = true;
            this.ResetW5x00.Click += new System.EventHandler(this.ResetW5x00_Click);
            // 
            // reset
            // 
            this.reset.Location = new System.Drawing.Point(7, 20);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(75, 23);
            this.reset.TabIndex = 0;
            this.reset.Text = "Reset";
            this.reset.UseVisualStyleBackColor = true;
            this.reset.Click += new System.EventHandler(this.reset_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(8, 194);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(120, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Dump Ethernet Config";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 278);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.controller);
            this.Controls.Add(this.all);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "Christmas Lights Master Control";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxLoopsBetweenBulbUpdates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxPacketsBetweenDisplay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox all;
        private System.Windows.Forms.ComboBox controller;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button sendMaxPacketsBetweenDisplay;
        private System.Windows.Forms.NumericUpDown maxPacketsBetweenDisplay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button dmx;
        private System.Windows.Forms.Button debug3;
        private System.Windows.Forms.Button debug2;
        private System.Windows.Forms.Button debug1;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Button debugOff;
        private System.Windows.Forms.Button debugON;
        private System.Windows.Forms.Button sendMaxLoopsBetweenBulbUpdates;
        private System.Windows.Forms.NumericUpDown maxLoopsBetweenBulbUpdates;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button ResetW5x00;
        private System.Windows.Forms.Button button4;
    }
}

