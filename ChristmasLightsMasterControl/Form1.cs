﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ChristmasLightsMasterControl
{
    public partial class Form1 : Form
    {
        const string __configfile = "ChristmasLightsMasterControl.xml";
        List<Controller> _controllers = new List<Controller>();

        public Form1()
        {
            InitializeComponent();

            XmlDocument config = new XmlDocument();
            try
            {
                config.Load(__configfile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + __configfile + " could not be loaded: " + ex.Message);
                Close();
                return;
            }

            try
            {
                foreach (XmlNode n in config.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "christmaslightsmastercontrol")
                        {
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (n1.Name.ToLower() == "controller")
                                    {
                                        Controller c = new Controller(n1);
                                        controller.Items.Add(c);
                                        _controllers.Add(c);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + __configfile + " could not be loaded due to content error: " + ex.Message);
                Close();
                return;
            }

            if (_controllers.Count == 0)
            {
                MessageBox.Show("Error: " + __configfile + " contained no controllers.");
                Close();
                return;
            }

            controller.SelectedIndex = 0;
        }

        void ValidateWindow()
        {
            if (all.Checked)
            {
                controller.Enabled = false;
            }
            else
            {
                controller.Enabled = true;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            ValidateWindow();

            maxPacketsBetweenDisplay.Value = ((Controller)controller.SelectedItem).MaxPacketsBetweenDisplay;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ValidateWindow();
        }

        void Send(int i, int j)
        {
            if(all.Checked)
            {
                foreach(Controller c in _controllers)
                {
                    c.Send(i, j);
                }
            }
            else
            {
                ((Controller)controller.SelectedItem).Send(i, j);
            }
        }

        void Send(int i)
        {
            Send(i, 0);
        }

        private void reset_Click(object sender, EventArgs e)
        {
            Send(255);
        }

        private void debug1_Click(object sender, EventArgs e)
        {
            Send(1);
        }

        private void debug2_Click(object sender, EventArgs e)
        {
            Send(2);
        }

        private void debug3_Click(object sender, EventArgs e)
        {
            Send(3);
        }

        private void dmx_Click(object sender, EventArgs e)
        {
            Send(0);
        }

        private void sendMaxPacketsBetweenDisplay_Click(object sender, EventArgs e)
        {
            if (all.Checked)
            {
                foreach(Controller c in _controllers)
                {
                    c.MaxPacketsBetweenDisplay = (int)maxPacketsBetweenDisplay.Value;
                }
            }
            else
            {
                ((Controller)controller.SelectedItem).MaxPacketsBetweenDisplay = (int)maxPacketsBetweenDisplay.Value;
            }

            Send(253, (int)maxPacketsBetweenDisplay.Value);
        }

        private void debugON_Click(object sender, EventArgs e)
        {
            Send(254, 1);
        }

        private void debugOff_Click(object sender, EventArgs e)
        {
            Send(254, 0);
        }

        private void sendMaxLoopsBetweenBulbUpdates_Click(object sender, EventArgs e)
        {
            if (all.Checked)
            {
                foreach (Controller c in _controllers)
                {
                    c.MaxLoopsBetweenBulbUpdates = (int)maxLoopsBetweenBulbUpdates.Value;
                }
            }
            else
            {
                ((Controller)controller.SelectedItem).MaxLoopsBetweenBulbUpdates = (int)maxLoopsBetweenBulbUpdates.Value;
            }

            Send(252, (int)maxLoopsBetweenBulbUpdates.Value);
        }

        private void ResetW5x00_Click(object sender, EventArgs e)
        {
            Send(249, 1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Send(250, 1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Send(250, 2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Send(250, 3);
        }

        private void controller_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Send(248);
        }
    }
}
