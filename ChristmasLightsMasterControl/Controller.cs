﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Net;
using System.Net.Sockets;

namespace ChristmasLightsMasterControl
{
    class Controller
    {
        int _maxPacketsBetweenDisplay = 1;
        int _maxLoopsBetweenBulbUpdates = -1;
        IPAddress _ipAddress = IPAddress.Parse("127.0.0.1");
        int _port = 5568;
        int _universe = 22222;
        string _name = string.Empty;
        IPEndPoint _endpoint;
        Socket _socket;
        byte[] _buffer = new byte[126 + 2 + 2];

        public override string ToString()
        {
            return "[" + _ipAddress.ToString() + "]  " + _name;
        }

        void SetInt(int offset, int value)
        {
            _buffer[offset] = (byte)((value & 0xFF00) >> 8);
            _buffer[offset+1] = (byte)(value & 0xFF);
        }

        public void Send(int i, int j)
        {
            SetInt(126, i);
            SetInt(128, j);
            _socket.SendTo(_buffer, _endpoint);
        }

        public Controller(XmlNode n0)
        {
            try
            {
                foreach (XmlNode n in n0.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "name")
                        {
                            _name = n.InnerText;
                        }
                        else if (n.Name.ToLower() == "maxpacketsbetweendisplay")
                        {
                            try
                            {
                                _maxPacketsBetweenDisplay = int.Parse(n.InnerText);
                            }
                            catch(Exception ex)
                            {
                                throw new ApplicationException("Error parsing MaxPacketsBetweenDisplay " + n.InnerText, ex);
                            }
                        }
                        else if (n.Name.ToLower() == "maxloopsbetweenbulbupdates")
                        {
                            try
                            {
                                _maxLoopsBetweenBulbUpdates = int.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error parsing MaxLoopsBetweenBulbUpdates " + n.InnerText, ex);
                            }
                        }
                        else if (n.Name.ToLower() == "ipaddress")
                        {
                            try
                            {
                                _ipAddress = IPAddress.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error parsing IPAddress " + n.InnerText, ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating controller.", ex);
            }

            _endpoint = new IPEndPoint(_ipAddress, _port);
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			_socket.Ttl = 1;

            _buffer[0] = 0x00;
            _buffer[1] = 0x10;

            _buffer[2] = 0x00;
            _buffer[3] = 0x00;

            _buffer[4] = 0x41;
            _buffer[5] = 0x53;
            _buffer[6] = 0x43;
            _buffer[7] = 0x2D;
            _buffer[8] = 0x45;
            _buffer[9] = 0x31;
            _buffer[10] = 0x2E;
            _buffer[11] = 0x31;
            _buffer[12] = 0x37;
            _buffer[13] = 0x00;
            _buffer[14] = 0x00;
            _buffer[15] = 0x00;

            //0x26D = 637 - 16
            _buffer[16] = 0x72;
            _buffer[17] = 0x6D;

            _buffer[18] = 0x00;
            _buffer[19] = 0x00;
            _buffer[20] = 0x00;
            _buffer[21] = 0x04;

            // inject unique id
            //CopyBytes(_buffer, 22, __uniqueid, 16);

            //0x257 = 637 - 38
            _buffer[38] = 0x72;
            _buffer[39] = 0x57;

            _buffer[40] = 0x00;
            _buffer[41] = 0x00;
            _buffer[42] = 0x00;
            _buffer[43] = 0x02;

            // inject sourcename
            //CopyBytes(_buffer, 44, __sourcename, 64);

            _buffer[108] = 101;

            _buffer[109] = 0x00;
            _buffer[110] = 0x00;

            _buffer[112] = 0x00;

            _buffer[113] = (byte)((_universe & 0xFF00) >> 8);
            _buffer[114] = (byte)(_universe & 0x00FF);

            //0x20A = 637 - 115
            _buffer[115] = 0x72;
            _buffer[116] = 0x0A;

            _buffer[117] = 0x02;

            _buffer[118] = 0xA1;

            _buffer[119] = 0x00;
            _buffer[120] = 0x00;

            _buffer[121] = 0x00;
            _buffer[122] = 0x01;

            _buffer[123] = 0x02;
            _buffer[124] = 0x01;

            //is this these right start code?
            _buffer[125] = 0x00;    
        }

        public int MaxPacketsBetweenDisplay
        {
            get
            {
                return _maxPacketsBetweenDisplay;
            }
            set
            {
                _maxPacketsBetweenDisplay = value;
            }
        }

        public int MaxLoopsBetweenBulbUpdates
        {
            get
            {
                return _maxLoopsBetweenBulbUpdates;
            }
            set
            {
                _maxLoopsBetweenBulbUpdates = value;
            }
        }

    }
}
