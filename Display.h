#ifndef DISPLAY_H
	#define DISPLAY_H
	
	#include "DMX.h"
	#ifdef UNIVERSE
		#include "BulbTracker.h"
	#endif
	#ifdef RELAYUNIVERSE
		#include "relays.h"	
	#endif

	class Display
	{
		DMX _dmx;
		#ifdef UNIVERSE
			BulbTracker _bulbTracker;
		#endif
		#ifdef RELAYUNIVERSE
			Relays _relays;
		#endif
		#ifdef DETECTMISSINGSEQUENCENUMBERS
			#ifdef RELAYUNIVERSE
				#ifdef UNIVERSE
					#define TOTALUNIVERSES OUTPUTSTRINGS + 1
				#else
					#define TOTALUNIVERSES 1
				#endif
			#else // assume UNIVERSE is defined
				#define TOTALUNIVERSES OUTPUTSTRINGS
			#endif
			short _lastSequence[TOTALUNIVERSES];
		#endif
		bool CheckStateChange(bool skip2, int packetSize);
		
		public:
		Display();
		void RouteDMX();
		void Process();
		void RunDebug(byte debug);
		void RunEthernetTest(byte testtype);
	};
#endif