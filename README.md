# README #

* This project is arduino code which allows an Arduino Mega with an Ethernet shield to run as an 8 output WS2811 DMX controller. It also supports relay/SSR outputs with the attachment of a relay/SSR board.
* The only file you need to change is global.h which has all the settings such as IP Address, Universes etc

Built and tested with W5100/W5200 and ENC28J60 ethernet shields. I have optimised it as much as I can. I usually use it with the ENC28J60 boards as they are cheaper and there were numerous quality issues 
with the chinese W5x00 boards (this may or may not still be true - the issue was the resistor networks on the ethernet connections were 10 times the value they should have been)

For the ENC28J60 please use the included UIPEthernet.zip library. This is an optimised library that minimises the overhead of dealing with network packets.

NOTE: These boards typically can only receive 2-3 universes reliably over ethernet before their receive buffers overflow and packets begin to be dropped. To get around this you need to spread out the 
arrival times of ethernet packets. One way to do this is in your sequencing software separate them such that they are not close together in the list of universes being sent out. If your sequencer/player 
supports packet deduplication this can also help. In extreme situations you can look to something like my e131 rate controller (https://bitbucket.org/keithsw1111/e131ratecontroller/src) which can run on 
a pi or a computer and take in the network packets from your show player and delay their delivery just enough to allow the arduino to keep up. I have run 8 universes reliably this way.

These days I use this code primarily for running a few SSRs/Relays where it is easy to use and efficient. The pixels while supported are an exercise in frustration which I would suggest you avoid unless you 
are a glutton for punishment. Alternatively you can look at Shelby Merricks ESP8266 ESPPixelStick project (https://github.com/forkineye/ESPixelStick) which will let you do about 4 universes out of one
output over wifi.