#ifndef RELAYS_H
	#define RELAYS_H
	
	#ifdef RELAYUNIVERSE
	
	#include "DMX.h"

	class Relays
	{
		#ifdef BATCHUPRELAYS
		byte _portA; // 22-29
		byte _portC; // 37-30
		byte _portD; // 21, 20, 19, 18, N/A, N/A, N/A ,38
		byte _portF; // Analog 0-7 54-61
		byte _portG; // 41, 40, 39, N/A, N/A, 4, N/A, N/A
		byte _portK; // Analog 8-15 62-69
		byte _portL; // 49-42
		byte SetBit(byte oldvalue, byte state, byte bit);
		byte DecodeRelay(int relay, byte* bit);
                byte _portSet;
		void EndBatch();
		#endif
		void SetRelay(int relay, byte state);

		public:
		Relays();
		void ProcessDMX(DMX* pDMX);
		void SetAllState(bool on);
		int RelayCount();
		void SetOneRelay(int relay, byte state);
	};
	
	#endif
	
#endif