#ifndef _FLASHSTRINGHELPER_H
	#define _FLASHSTRINGHELPER_H

	#include "global.h"

	// Class for accessing flash stored strings as regular character strings
	class FlashStringHelper
	{
	private:
		char* buffer; // buffer where the string is copied to

	public:
		static short Length(const __FlashStringHelper* ps); // calculate the length of the string in flash memory
		// create/destroy the string
		FlashStringHelper(const __FlashStringHelper* ps);
		~FlashStringHelper();
		
		// Access the string as a char string
		char* GetString();
	};

#endif // _FLASHSTRINGHELPER_H
