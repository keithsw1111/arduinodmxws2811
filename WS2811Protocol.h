/*
 * WS2811 LED driver.
 */

// Data Sheet Timings
// 0 - .25us high + 1.0us low
// 1 - .625us high + .625us low
// first .25us always high ... next .375us the value ... next .625us always low

// Non Data Sheet Timings
// 0 - .25us high + 1.0us low
// 1 - 1.0us high + .25us low
// first .25us always high ... next .75us the value ... next .25us always low

// 4 cycles = 0.25us ... 1 cycle = 0.0625us .. .375us = 6 cycles .625 = 10 cycles

#ifndef WS2811PROTOCOL_H

	#define WS2811PROTOCOL_H

	//#define TESTTIMINGS 1
	
	#ifdef TESTTIMINGS
	
			// len is the number of bytes of data ... this needs to be wrapped with a high prefix and low suffix.
			#define WS2811PROTOCOL_IO(PORT, RGB, LEN) \
				asm volatile( \
				"    cp %A[len], r1\n" \
				"    cpc %B[len], r1\n" \
				"    brne 1f\n" \
				"    rjmp 4f\n" \
				"1:  in r26, __SREG__\n" \
				"    cli\n" \
				"    ldi r20, 255\n" \
				"    ldi r21, 0\n" \
				"2:  out %[port], r20       ; 1 (hold for 4 cycles - 0.25 us)\n" \
				"    ld r19, Z+             ; 2 (maybe 1 on mega in which case i might need another nop) \n" \
				"    nop                    ; 1 \n" \
				"    out %[port], r21       ; 1 (hold for 6 cycles - 0.375us) \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    sbiw %A[len], 1        ; 2 \n" \
				"    out %[port], r21       ; 1 (hold for 10 cycles - 0.625us) \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    breq 3f                ; 1 if false which is the only one that matters (2 if true) \n" \
				"    rjmp 2b                ; 2 \n" \
				"3:  out __SREG__, r26\n" \
				"4:\n" \
				: \
				: [rgb] "z" (RGB), \
				  [len] "w" (LEN), \
				  [port] "I" (_SFR_IO_ADDR(PORT)) \
				: "r19", "r20", "r21", "r26", "r27", "cc", "memory" \
				)
				
	#else
	
		#ifdef USEDATASHEETTIMINGS
		
			// len is the number of bytes of data ... this needs to be wrapped with a high prefix and low suffix.
			#define WS2811PROTOCOL_IO(PORT, RGB, LEN) \
				asm volatile( \
				"    cp %A[len], r1\n" \
				"    cpc %B[len], r1\n" \
				"    brne 1f\n" \
				"    rjmp 4f\n" \
				"1:  in r26, __SREG__\n" \
				"    cli\n" \
				"    ldi r20, 255\n" \
				"    ldi r21, 0\n" \
				"2:  out %[port], r20       ; 1 (hold for 4 cycles - 0.25 us)\n" \
				"    ld r19, Z+             ; 2 (maybe 1 on mega in which case i might need another nop) \n" \
				"    nop                    ; 1 \n" \
				"    out %[port], r19       ; 1 (hold for 6 cycles - 0.375us) \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    sbiw %A[len], 1        ; 2 \n" \
				"    out %[port], r21       ; 1 (hold for 10 cycles - 0.625us) \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    breq 3f                ; 1 if false which is the only one that matters (2 if true) \n" \
				"    rjmp 2b                ; 2 \n" \
				"3:  out __SREG__, r26\n" \
				"4:\n" \
				: \
				: [rgb] "z" (RGB), \
				  [len] "w" (LEN), \
				  [port] "I" (_SFR_IO_ADDR(PORT)) \
				: "r19", "r20", "r21", "r26", "r27", "cc", "memory" \
				)

		#else

			// len is the number of bytes of data ... this needs to be wrapped with a high prefix and low suffix.
			#define WS2811PROTOCOL_IO(PORT, RGB, LEN) \
				asm volatile( \
				"    cp %A[len], r1\n" \
				"    cpc %B[len], r1\n" \
				"    brne 1f\n" \
				"    rjmp 4f\n" \
				"1:  in r26, __SREG__\n" \
				"    cli\n" \
				"    ldi r20, 255\n" \
				"    ldi r21, 0\n" \
				"2:  out %[port], r20       ; 1 (hold for 4 cycles - 0.25 us)\n" \
				"    ld r19, Z+             ; 2 (maybe 1 on mega in which case i might need another nop) \n" \
				"    nop                    ; 1 \n" \
				"    out %[port], r19       ; 1 (hold for 12 cycles - 0.75us) \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    nop                    ; 1 \n" \
				"    sbiw %A[len], 1        ; 2 \n" \
				"    out %[port], r21       ; 1 (hold for 4 cycles - 0.25us) \n" \
				"    breq 3f                ; 1 if false which is the only one that matters (2 if true) \n" \
				"    rjmp 2b                ; 2 \n" \
				"3:  out __SREG__, r26\n" \
				"4:\n" \
				: \
				: [rgb] "z" (RGB), \
				  [len] "w" (LEN), \
				  [port] "I" (_SFR_IO_ADDR(PORT)) \
				: "r19", "r20", "r21", "r26", "r27", "cc", "memory" \
				)
		
		#endif

	#endif
		
	#define DEFINE_WS2811PROTOCOL_FN_IO(NAME, PORT) \
	extern void NAME(byte *rgb, uint16_t len) __attribute__((noinline)); \
	void NAME(byte *rgb, uint16_t len) { WS2811PROTOCOL_IO(PORT, rgb, len); }

#endif 

