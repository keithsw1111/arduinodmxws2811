#include "global.h"
#include "colourmapping.h"
//#include "timer.h"

// Only need this if we have WS2811 bulbs
#ifdef UNIVERSE
	#include "BulbTracker.h"

	#ifdef BULBMULTIPLIER
		#include "bulbmultiplier.h"
	#endif

	#define BITSPERCOLOUR 8
	// these allow us to optionally pack > 1 string in a universe
	// basically it puts as many whole strings into a universe as fits
	#ifdef ONEUNIVERSEPEROUTPUT
		#define _UNIVERSES OUTPUTSTRINGS
		#define _STRINGSPERUNIVERSE 1
		#define _STRINGSINLASTUNIVERSE 1
	#else
		#define _UNIVERSES ((OUTPUTSTRINGS + (512/(MAXBULBS * 3)))/ (512/(MAXBULBS * 3)))
		#define _STRINGSPERUNIVERSE (512/(MAXBULBS*3))
		#define _STRINGSINLASTUNIVERSE (OUTPUTSTRINGS - ((_UNIVERSES-1)*_STRINGSPERUNIVERSE))
	#endif
	#define _LASTUNIVERSE(x) ((x-UNIVERSE) == _UNIVERSES-1)

	BulbTracker::BulbTracker()
	{
		Utility::Debugln(F("BulbTracker create."));
		Utility::Debug(F("_UNIVERSES "));
		Utility::Debugln(_UNIVERSES);
		Utility::Debug(F("_STRINGSPERUNIVERSE "));
		Utility::Debugln(_STRINGSPERUNIVERSE);
		Utility::Debug(F("_STRINGSINLASTUNIVERSE "));
		Utility::Debugln(_STRINGSINLASTUNIVERSE);

		// set output pins
		DIRECTIONREGISTER = 0xFF;

		_pBulbData = (byte*)malloc(MAXBULBS * OUTPUTSTRINGS * 3);
		memset(_pBulbData, 0x00, MAXBULBS * OUTPUTSTRINGS * 3);

		//Utility::Debug(F("BulbTracker::Bulbs data array allocated "));
		//Utility::Debugln((long)_pBulbData, HEX);

		_pSendBuffer = (byte*)malloc(MAXBULBS * 3 * BITSPERCOLOUR);
		memset(_pSendBuffer, 0x00, MAXBULBS * BITSPERCOLOUR * 3);

		#ifdef TRACKCHANGES
			// All bulbs initially need to be sent
			SetAllDirty();
		#endif

		Utility::Debugln(F("BulbTracker create done!"));
	}
			
	void BulbTracker::DumpBulbData()
	{
		Utility::Debugln(F("Bulb data ... dumped by OUTPUTSTRINGS."));
		for (byte j = 0; j < OUTPUTSTRINGS; j++)
		{
			Utility::Debug(F("String "));
			Utility::Debug(j);
			Utility::Debug(F(" Universe "));
			Utility::Debug(UNIVERSE + j);
			Utility::Debug(F(" Bulb Data "));
			byte* p = _pBulbData + j;

			for (byte i = 0; i < MAXBULBS; i++)
			{
				Utility::Debug(F("("));
				Utility::Debug(*(p+i*3*OUTPUTSTRINGS), HEX);
				Utility::Debug(F(","));
				Utility::Debug(*(p+i*3*OUTPUTSTRINGS+OUTPUTSTRINGS), HEX);
				Utility::Debug(F(","));
				Utility::Debug(*(p+i*3*OUTPUTSTRINGS+OUTPUTSTRINGS+OUTPUTSTRINGS), HEX);
				Utility::Debug(F(") "));
			}
			Utility::Debugln(F(""));
		}
	}
	void BulbTracker::DumpSendBuffer()
	{
		Utility::Debug(F("Send buffer: "));

		for (byte i = 0; i < MAXBULBS; i++)
		{
			for (byte j = 0; j < 3; j ++)
			{
				for ( byte k = 0; k < BITSPERCOLOUR; k++)
				{
					Utility::Debug(*(_pSendBuffer+(i*3*BITSPERCOLOUR+j*BITSPERCOLOUR)+k), HEX);
					Utility::Debug(F(" "));
				}
				Utility::Debug(F(","));
			}
			Utility::Debugln(F(""));
		}

	}

	#ifdef TRACKCHANGES
	void BulbTracker::DumpTracker()
	{
		Utility::Debugln(F("Bulb clean/Dirty flags ... 1 per bulb ... '1' if that bulb on any string has been updated."));
		for(byte i=0; i<MAXBULBS;i++)
		{
			Utility::Debug(_tracker[i]);
			Utility::Debug(F(" "));
		}
		Utility::Debugln(F(""));
	}
	#endif

	void BulbTracker::Dump()
	{
		DumpBulbData();
		#ifdef TRACKCHANGES
		DumpTracker();
		#endif
		DumpSendBuffer();
	}

	void BulbTracker::SetBulbColour(byte r, byte g, byte b, byte string, short bulb)
	{
		#ifdef DEBUGSERIAL
			//Utility::Debug(F("BulbTracker::SetBulbColour. String "));
			//Utility::Debug(string);
			//Utility::Debug(F(" Bulb "));
			//Utility::Debug(bulb);
			//Utility::Debug(F(" ("));
			//Utility::Debug(r, HEX);
			//Utility::Debug(F(","));
			//Utility::Debug(g, HEX);
			//Utility::Debug(F(","));
			//Utility::Debug(b, HEX);
			//Utility::Debugln(F(")"));
		#endif

		#ifdef TRACKCHANGES
			// our bulb will change
			SetDirty(bulb);
		#endif

		byte* p = _pBulbData + string + (bulb * OUTPUTSTRINGS * 3);
		*p = r;
		*(p+OUTPUTSTRINGS) = g;
		*(p+OUTPUTSTRINGS+OUTPUTSTRINGS) = b;

		#ifdef DEBUGSERIAL
			//DumpBulbData();
		#endif
	}

	void BulbTracker::SetStringColour(byte r, byte g, byte b, byte string)
	{
		#ifdef DEBUGSERIAL
			//Utility::Debug(F("BulbTracker::SetStringColour. String "));
			//Utility::Debug(string);
			//Utility::Debug(F(" ("));
			//Utility::Debug(r, HEX);
			//Utility::Debug(F(","));
			//Utility::Debug(g, HEX);
			//Utility::Debug(F(","));
			//Utility::Debug(b, HEX);
			//Utility::Debugln(F(")"));
		#endif
		
		#ifdef TRACKCHANGES
			// make them all dirty ... every bulb will change
			SetAllDirty();
		#endif

		byte* p = _pBulbData + string;
		for (byte i = 0; i < MAXBULBS; i++)
		{

			*p = r;
			*(p+OUTPUTSTRINGS) = g;
			*(p+OUTPUTSTRINGS+OUTPUTSTRINGS) = b;
			p += OUTPUTSTRINGS * 3;
		}			

		#ifdef DEBUGSERIAL
			//DumpBulbData();
		#endif
	}

	void BulbTracker::SetAllColour(byte r, byte g, byte b)
	{
		#ifdef DEBUGSERIAL
			//Utility::Debug(F("BulbTracker::SetAllColour. ("));
			//Utility::Debug(r, HEX);
			//Utility::Debug(F(","));
			//Utility::Debug(g, HEX);
			//Utility::Debug(F(","));
			//Utility::Debug(b, HEX);
			//Utility::Debugln(F(")"));
		#endif
		
		#ifdef TRACKCHANGES
			// make them all dirty ... every bulb will change
			SetAllDirty();
		#endif

		byte* p = _pBulbData;
		for (byte i = 0; i < MAXBULBS; i++)
		{
			memset(p, r, OUTPUTSTRINGS);
			memset(p+OUTPUTSTRINGS, g, OUTPUTSTRINGS);
			memset(p+OUTPUTSTRINGS+OUTPUTSTRINGS, b, OUTPUTSTRINGS);
			p += OUTPUTSTRINGS * 3;
		}			

		#ifdef DEBUGSERIAL
			//DumpBulbData();
		#endif
	}

	DEFINE_TRANSPOSEBULBDATA_FN(TRANSPOSEBULBDATA)
	DEFINE_WS2811PROTOCOL_FN_IO(MYWS2811PROTOCOLP, PINPORT)

	void BulbTracker::Display()
	{
		#ifdef DEBUGSERIAL
			Utility::Debugln(F("BulbTracker::Display. **********************"));
			Utility::Debug(F("F"));
			Utility::Debugln(Utility::FreeRam());
		#endif

		#ifdef DEBUGSERIAL
			//DumpBulbData();
		#endif
		for (byte i = 0; i < MAXBULBS; i++)
		{
			#ifdef TRACKCHANGES
			if (IsDirty(i))
			{
			#endif
				#ifdef DEBUGSERIAL
					//Utility::Debug(F("BulbTracker:: Pre transpose bulb "));
					//Utility::Debugln(i);
					//Utility::Debug(F("BulbData: "));
					//for (byte k = 0; k < OUTPUTSTRINGS*3; k++)
					//{
					//	Utility::Debug(*(_pBulbData + (i * OUTPUTSTRINGS * 3) + k), HEX);
					//	Utility::Debug(F(" "));
					//}
					//Utility::Debugln(F(""));
				#endif

				// Copy across the updated data into the send buffer
				TRANSPOSEBULBDATA(_pBulbData + (i * OUTPUTSTRINGS * 3), _pSendBuffer + (i * BITSPERCOLOUR * 3), OUTPUTSTRINGS);

			#ifdef TRACKCHANGES
			}
			#endif
		}

		#ifdef DEBUGSERIAL
			//DumpSendBuffer();
		#endif
		MYWS2811PROTOCOLP(_pSendBuffer, 3 * BITSPERCOLOUR * MAXBULBS);
		#ifdef TRACKCHANGES
			SetAllClean();
		#endif
	}

	bool BulbTracker::ProcessDMX(DMX* pDMX)
	{
		//Timer process2811("   process2811");
		// check this universe is one we understand
		short universe = pDMX->Universe();
		if (universe < UNIVERSE || universe > UNIVERSE + _UNIVERSES - 1)
		{
			Utility::Debugln(F("    BulbTracker universe rejected ... not one we are processing."));
      Utility::Debug(F("        Universe received: "));
      Utility::Debugln(universe);
      Utility::Debug(F("        Start universe: "));
      Utility::Debugln(UNIVERSE);
      Utility::Debug(F("        End Universe: "));
      Utility::Debugln(UNIVERSE + _UNIVERSES - 1);
			return false;
		}

		#ifdef DEBUGSERIAL
			Utility::Debug(F("BulbTracker processing DMX packet for universe "));
			Utility::Debugln(pDMX->Universe());
			Utility::Debug(F("Last Universe? "));
			Utility::Debugln(_LASTUNIVERSE(universe));
		#endif

		#ifdef DEBUGDUMPPIN
		bool dump = !digitalRead(DEBUGDUMPPIN);
		#endif

		byte rgb[3];
		short inputchannel = 1 - 3;
		
		#ifdef DEBUGDUMPPIN
			if (dump)
			{
				Utility::Debug(F("    DMX Packet dump for universe "));
				Utility::Debugln(universe);
			}
		#endif

		// for each string packed into this universe
		for (byte j = 0;  j < _STRINGSPERUNIVERSE; j++) 
		{
			if (_LASTUNIVERSE(universe) && j >= _STRINGSINLASTUNIVERSE)
			{
				// this does not exist
				break;
			}
		
			#ifdef DEBUGSERIAL
				Utility::Debug(F("    String in universe "));
				Utility::Debugln((universe - UNIVERSE) * _STRINGSPERUNIVERSE + j);
			#endif
			
			// calculate the string pointer
			byte* p = _pBulbData + (universe - UNIVERSE) * _STRINGSPERUNIVERSE + j;
		
			byte bulb  = 0;
			
			while (bulb < MAXBULBS)
			{
				if (pDMX->Read(&rgb[0], sizeof(rgb)))
				{
					inputchannel += sizeof(rgb);
					
					for (int ptol = 0; ptol < PHYSICALTOLOGICALRATIO; ptol++)
					{
						byte* p2 = p + bulb * OUTPUTSTRINGS * 3;
					
						#ifdef DEBUGSERIAL
							//Utility::Debug(F("BulbTracker::Setting bulb "));
							//Utility::Debug(i);
							//Utility::Debug(F(" to "));
							//Utility::Debug(r, HEX);
							//Utility::Debug(F(","));
							//Utility::Debug(g, HEX);
							//Utility::Debug(F(","));
							//Utility::Debugln(b, HEX);
						#endif

						byte r;
						byte g;
						byte b;

						byte colourorder = pgm_read_byte_near(UniverseColourMap + universe - UNIVERSE);
						short changeat = pgm_read_word_near(UniverseColourMapChangesAt + universe - UNIVERSE);
						if (changeat > 0 && bulb >= changeat)
						{
							colourorder = pgm_read_byte_near(UniverseColourMapChangesTo + universe - UNIVERSE);
						}
						
						// this works but the var names are wrong. rgb[0] is always red ... but assigning it to b or g just indicates that the output it muddled
						switch(colourorder)
						{
							case RGB:
								r = rgb[0];
								g = rgb[1];
								b = rgb[2];
								break;
							case RBG:
								r = rgb[0];
								b = rgb[1];
								g = rgb[2];
								break;
							case GRB:
								g = rgb[0];
								r = rgb[1];
								b = rgb[2];
								break;
							case GBR:
								g = rgb[0];
								b = rgb[1];
								r = rgb[2];
								break;
							case BGR:
								b = rgb[0];
								g = rgb[1];
								r = rgb[2];
								break;
							case BRG:
								b = rgb[0];
								r = rgb[1];
								g = rgb[2];
								break;
						}
						
						#ifdef DEBUGDUMPPIN
							if (dump)
							{
								Utility::Debug(F("("));
								Utility::Debug(r, HEX);
								Utility::Debug(F(","));
								Utility::Debug(g, HEX);
								Utility::Debug(F(","));
								Utility::Debug(b, HEX);
								Utility::Debug(F(")"));
							}
						#endif

						if (*(p2) != r ||
							*(p2 + OUTPUTSTRINGS) != g ||
							*(p2 + OUTPUTSTRINGS + OUTPUTSTRINGS) != b)
						{
							#ifdef TRACKCHANGES
								SetDirty(bulb);
							#endif
							*(p2) = r;
							*(p2 + OUTPUTSTRINGS) = g;
							*(p2 + OUTPUTSTRINGS + OUTPUTSTRINGS) = b;

							#ifdef DEBUGDUMPPIN
								if (dump)
								{
									Utility::Debug(F("*"));
								}
							#endif
						}
						bulb++;
					}
					
					#ifdef BULBMULTIPLIER
						short bm = 0;
						short sourcechannel = pgm_read_word_near(BulbMultiplier+bm);
						
						while (sourcechannel != 0)
						{						
							if (sourcechannel == inputchannel)
							{
								// do the bulb multiply
								short outputchannel = pgm_read_word_near(BulbMultiplier+bm+1);
								short bulbs = pgm_read_word_near(BulbMultiplier+bm+2);

								#ifdef DEBUGSERIAL
									Utility::Debug(F("    Multiplying input "));
									Utility::Debug(sourcechannel);
									Utility::Debug(F(" to channel "));
									Utility::Debug(outputchannel);
									Utility::Debug(F(" bulbs "));
									Utility::Debugln(bulbs);
								#endif
				
								for (short bb = 0; bb < bulbs; bb++)
								{
									byte* p2 = p + ((outputchannel-1)/3+bb) * OUTPUTSTRINGS * 3;
							
									byte r;
									byte g;
									byte b;
									
									// this works but the var names are wrong. rgb[0] is always red ... but assigning it to b or g just indicates that the output it muddled
									switch(pgm_read_byte_near(UniverseColourMap + universe - UNIVERSE))
									{
										case RGB:
											r = rgb[0];
											g = rgb[1];
											b = rgb[2];
											break;
										case RBG:
											r = rgb[0];
											b = rgb[1];
											g = rgb[2];
											break;
										case GRB:
											g = rgb[0];
											r = rgb[1];
											b = rgb[2];
											break;
										case GBR:
											g = rgb[0];
											b = rgb[1];
											r = rgb[2];
											break;
										case BGR:
											b = rgb[0];
											g = rgb[1];
											r = rgb[2];
											break;
										case BRG:
											b = rgb[0];
											r = rgb[1];
											g = rgb[2];
											break;
									}
								
									if (*(p2) != r ||
										*(p2 + OUTPUTSTRINGS) != g ||
										*(p2 + OUTPUTSTRINGS + OUTPUTSTRINGS) != b)
									{
										#ifdef TRACKCHANGES
											SetDirty(bulb);
										#endif
										*(p2) = r;
										*(p2 + OUTPUTSTRINGS) = g;
										*(p2 + OUTPUTSTRINGS + OUTPUTSTRINGS) = b;

										#ifdef DEBUGDUMPPIN
											if (dump)
											{
												Utility::Debug(F("*"));
											}
										#endif
									}							
								}
							}						
							bm += 3;
							sourcechannel = pgm_read_word_near(BulbMultiplier+bm);
						}
					#endif
				}
				else
				{
					// we are out of packet data so exit
					break;
				}
			}
		}
		
		#ifdef DEBUGDUMPPIN
			if (dump)
			{
				Utility::Debugln(F(""));
			}
		#endif

		#ifdef DEBUGSERIAL
			Utility::Debugln(F("    BulbTracker::done processing DMX Message."));
		#endif
		
		//process2811.DumpSerial();
		
		// we handled this packer
		return true;
	}
#endif
