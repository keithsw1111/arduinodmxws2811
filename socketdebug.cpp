#include "global.h"
#include "socketdebug.h"

#ifdef USEENC28J60
void socketDumpStateAll(byte level)
{
	socketDumpState(0, level);
}

void socketDumpState(SOCKET s, byte level)
{
	Serial.println(F("Socket Dump State not implemented."));
}
#else

#include <utility/w5100.h>

void socketDumpStateAll(byte level)
{
	for (byte i = 0; i < W5100.getMaxSockets(); i++)
	{
		socketDumpState(i, level);
	}
}

void socketDumpState(SOCKET s, byte level)
{
	Serial.print(F("Socket: "));
	Serial.print(s);

	SPI.beginTransaction(SPI_ETHERNET_SETTINGS);

	// W5100 & W5200 common items
	// Mode (Sn_MR) 0x0(n+4)00 - lvl 0
	byte mr = W5100.readSnMR(s);
	Serial.print(F(",Mode "));
	Serial.print(mr, BIN);
	
	// Status (Sn_SR) 0x0(n+4)03 - lvl 0
	byte sr = W5100.readSnSR(s);
	Serial.print(F(",Status "));
	Serial.print(sr, HEX);

	// Source Port (Sn_PORT) 0x0(n+4)04-0x0(n+4)05 - lvl 0
	uint16_t port = W5100.readSnPORT(s);
	Serial.print(F(",Port "));
	Serial.print(port);

	// Max Segment Size (Sn_MSSR) 0x0(n+4)12-0x0(n+4)13 - lvl 0
	uint16_t mssr = W5100.readSnMSSR(s);
	Serial.print(F(",Max Segment Size "));
	Serial.print(mssr);

	// Transmit Free Size (Sn_TX_FSR) 0x0(n+4)20- 0x0(n+4)21 - lvl 0
	uint16_t txfsr = W5100.readSnTX_FSR(s);
	Serial.print(F(",TX Free Size "));
	Serial.print(txfsr);

	// Transmit Read Pointer (Sn_TX_RD) 0x0(n+4)22-(n+4)23 - lvl 0
	uint16_t txrd = W5100.readSnTX_RD(s);
	Serial.print(F(",TX Read Ptr "));
	Serial.print(txrd);
	
	// Transmit Write Pointer (Sn_TX_WR) 0x0(n+4)24-0x0(n+4)25 - lvl 0
	uint16_t txwr = W5100.readSnTX_WR(s);
	Serial.print(F(",TX Write Ptr "));
	Serial.print(txwr);
	
	// Receive Size (Sn_RX_RSR) 0x0(n+4)26-0x0(n+4)27 - lvl 0
	uint16_t rxrsr = W5100.readSnRX_RSR(s);
	Serial.print(F(",RX Received Size "));
	Serial.print(rxrsr);

	// Receive Read Pointer (Sn_RX_RD) 0x0(n+4)28-0x0(n+4)29 - lvl 0
	uint16_t rxrd = W5100.readSnRX_RD(s);
	Serial.print(F(",RX Read Ptr 0x"));
	Serial.print(rxrd, HEX);
	
	if (level > 0)
	{
		// Interrupt (Sn_IR) 0x0(n+4)02 - lvl 1
		byte ir = W5100.readSnIR(s);
		Serial.print(F(",Interrupt "));
		Serial.print(ir, BIN);

		// Destination IP Address (Sn_DIPR) 0x0(n+4)0C-0x0(n+4)0F - lvl 1
		unsigned long dipr;
		W5100.readSnDIPR(s, (byte*)&dipr);
		Serial.print(F(",Dest IP Address "));
		for (int i = 0; i < sizeof(dipr); i++)
		{
			Serial.print(*(((byte*)&dipr) + i));
			Serial.print(F("."));
		}
		
		// Destination Port (Sn_DPORT) 0x0(n+4)10-0x0(n+4)11 - lvl 1
		uint16_t dport = W5100.readSnDPORT(s);
		Serial.print(F(",Dest Port "));
		Serial.print(dport);
		
		// TOS (Sn_TOS) 0x0(n+4)15 - lvl 1
		byte tos = W5100.readSnTOS(s);
		Serial.print(F(",TOS "));
		Serial.print(tos);

		// TTL (Sn_TTL) 0x0(n+4)16 - lvl 1
		byte ttl = W5100.readSnTTL(s);
		Serial.print(F(",TTL "));
		Serial.print(ttl);
	}
	
	if (level > 1)
	{
		// Destination MAC (Sn_DHAR) 0x0(n+4)06-0x0(n+4)0B - lvl 2
		byte dhar[6];
		W5100.readSnDHAR(s, (byte*)&dhar);
		Serial.print(F(",Dest MAC Address: "));
		for (int i = 0; i < sizeof(dhar); i++)
		{
			Serial.print(*(((byte*)&dhar[0]) +i), HEX);
			Serial.print(F(":"));
		}
		
		// Protocol in IP Raw Mode (Sn_PROTO) 0x0(n+4)14 - lvl 2
		byte proto = W5100.readSnPROTO(s);
		Serial.print(F(",Protocol in Raw Mode "));
		Serial.print(proto);
	}
	if (level > 3)
	{
		// Command (Sn_CR) 0x0(n+4)01 - lvl 4
		byte cr = W5100.readSnCR(s);
		Serial.print(F(",Command "));
		Serial.print(cr);
	}
		
	// W5100 only items
	
	// W5200 only items
	// RX Memory Size (Sn_RXMEM_SIZE) 0x0(n+4)1E - lvl 0
	byte rxmem_size = W5100.readSnRXBUF_SIZE(s);
	Serial.print(F(",RX Memory Size "));
	Serial.print(rxmem_size);
	
	// TX Memory Size (Sn_TXMEM_SIZE) 0x0(n+4)1F - lvl 0
	byte txmem_size = W5100.readSnTXBUF_SIZE(s);
	Serial.print(F(",TX Memory Size "));
	Serial.print(txmem_size);

	// Receive Write Pointer (Sn_RX_WR) 0x0(n+4)2A-0x0(n+4)2B - lvl 0
	uint16_t rxwr = W5100.readSnRX_WR(s);
	Serial.print(F(",Rx Write Ptr** 0x"));
	Serial.print(rxwr, HEX);
	
	if (level > 0)
	{
		// Interrupt Mask (Sn_IMR) 0x0(n+4)2C - lvl 1
		byte imr = W5100.readSnIMR(s);
		Serial.print(F(",Interrupt Mask "));
		Serial.print(imr);
	}
	
	if (level > 2)
	{
		// Fragment Offset in IP Header (Sn_FRAG) 0x0(n+4)2D-0x0(n+4)2E - lvl 3
		uint16_t frag = W5100.readSnFRAG(s);
		Serial.print(F(",Fragment Offset 0x"));
		Serial.print(frag, HEX);
	}

	Serial.println(F(""));

	SPI.endTransaction();
}
#endif