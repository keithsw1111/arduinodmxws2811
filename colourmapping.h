#ifndef COLOURMAPPER_H
	#define COLOURMAPPER_H

	#ifdef UNIVERSE

	// #defines for colour order when processing universe
	#define RGB 0
	#define GRB 1
	#define RBG 2
	#define GBR 3
	#define BRG 4
	#define BGR 5

	// For each WS2811 universe this specifies the colour order the string attached to the output expects
	// Assumes the data arriving in the E131 packet is always in RGB order
	
	const char UniverseColourMap[] PROGMEM = { RGB, // this is for UNIVERSE as defined in gloabl.h
											   RGB, // this is for UNIVERSE + 1
											   RGB,
											   RGB,
											   RGB,
											   RGB,
											   RGB,
											   RGB }; // this is for UNIVERSE + 7

	const short UniverseColourMapChangesAt[] PROGMEM = { 0, 0, 0, 0, 0, 0, 0, 0 };										   

	const char UniverseColourMapChangesTo[] PROGMEM = { RGB, // this is for UNIVERSE as defined in gloabl.h
											   RGB, // this is for UNIVERSE + 1
											   RGB,
											   RGB,
											   RGB,
											   RGB,
											   RGB,
											   RGB }; // this is for UNIVERSE + 7
	
	#endif
	
#endif
