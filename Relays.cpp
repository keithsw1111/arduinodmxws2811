#include "global.h"

#ifdef RELAYUNIVERSE

#include "Relays.h"
#include "RelayMapping.h"

int Relays::RelayCount()
{
	return sizeof(RelayMap);
}

#ifdef BATCHUPRELAYS
void Relays::EndBatch()
{
	if (_portSet & (1 << PA))
	{
		PORTA = _portA;
	}
	if (_portSet & (1 << PC))
	{
		PORTC = _portC;
	}
	if (_portSet & (1 << PD))
	{
		PORTD = _portD;
	}
	if (_portSet & (1 << PF))
	{
		PORTF = _portF;
	}
	if (_portSet & (1 << PG))
	{
		PORTG = _portG;
	}
	if (_portSet & (1 << PK))
	{
		PORTK = _portK;
	}
	if (_portSet & (1 << PL))
	{
		PORTL = _portL;
	}

	_portSet = 0;
}

byte Relays::SetBit(byte old, byte state, byte bit)
{
	#ifdef INVERTRELAYOUTPUT
		if (state > 0)
		{
			// 0 = ON
			return (old & (0xFF - (1 << bit)));
		}
		else
		{
			// 1 = OFF
			return (old | (1 << bit));
		}
	#else
		if (state == 0)
		{
			// 0 = OFF
			return (old & (0xFF - (1 << bit)));
		}
		else
		{
			// 1 = ON
			return (old | (1 << bit));
		}
	#endif
}

byte Relays::DecodeRelay(int relay, byte* bit)
{
    *bit = pgm_read_byte_near(RelayPortPinMap + relay);
    return pgm_read_byte_near(RelayPortMap + relay);	
}

#endif

void Relays::SetRelay(int relay, byte state)
{
	#ifdef DEBUGSERIAL
		Utility::Debug(F("    Setting relay "));
		Utility::Debug(relay);
		Utility::Debug(F(" to "));
		Utility::Debugln(state);
	#endif

	#ifdef BATCHUPRELAYS
		byte bit;
		switch(DecodeRelay(relay, &bit))
		{
			case PA:
				_portA = SetBit(_portA, state, bit);
				_portSet |= (1 << PA);
				break;
			case PC:
				_portC = SetBit(_portC, state, bit);
				_portSet |= (1 << PC);
				break;
			case PD:
				_portD = SetBit(_portD, state, bit);
				_portSet |= (1 << PD);
				break;
			case PF:
				_portF = SetBit(_portF, state, bit);
				_portSet |= (1 << PF);
				break;
			case PG:
				_portG = SetBit(_portG, state, bit);
				_portSet |= (1 << PG);
				break;
			case PK:
				_portK = SetBit(_portK, state, bit);
				_portSet |= (1 << PK);
				break;
			case PL:
				_portL = SetBit(_portL, state, bit);
				_portSet |= (1 << PL);
				break;
			default:
				Utility::Debug(F("Unknown relay:"));
				Utility::Debugln(relay);
				break;
		}
	#else
		digitalWrite(pgm_read_byte_near(RelayMap + relay), state);
	#endif
}

void Relays::SetOneRelay(int relay, byte state)
{
	SetRelay(relay, state);
	#ifdef BATCHUPRELAYS
		EndBatch();
	#endif
}

Relays::Relays()
{
	Utility::Debugln(F("Relays created."));
	
	_portSet = 0;

	for (byte i=0; i < sizeof(RelayMap); i++)
	{
		pinMode(pgm_read_byte_near(RelayMap + i), OUTPUT);
		SetRelay(i, 0);
	}	
	#ifdef BATCHUPRELAYS
		EndBatch();
	#endif
}

void Relays::SetAllState(bool on)
{
	for (byte i=0; i < sizeof(RelayMap); i++)
	{
		SetRelay(i, on);
	}
	#ifdef BATCHUPRELAYS
		EndBatch();
	#endif
}
		
void Relays::ProcessDMX(DMX* pDMX)
{
	short left = pDMX->Left();
	#ifdef DEBUGSERIAL
		Utility::Debug(F("Relays processing DMX packet "));
		Utility::Debug(left);
		Utility::Debugln(F(" channels to process."));
	#endif

	for(byte i=0; i < min(sizeof(RelayMap), left); i++)
	{
		bool state = (pDMX->Read() > 0);
		
		#ifdef DEBUGSERIAL
			//Utility::Debug(F("Setting relay "));
			//Utility::Debug(i);
			//Utility::Debug(F(" on pin "));
			//Utility::Debug(pgm_read_byte_near(RelayMap + i));
			//Utility::Debug(F(" to "));
			//Utility::Debugln(state);
		#endif

		SetRelay(i, state);
	}
	#ifdef BATCHUPRELAYS
		EndBatch();
	#endif

	#ifdef DEBUGSERIAL
		Utility::Debugln(F("Relay done processing DMX Message."));
	#endif
}

#endif
