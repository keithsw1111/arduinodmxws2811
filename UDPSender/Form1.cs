﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace UDPSender
{
    public partial class Form1 : Form
    {
        short _sequence = 19;
        bool _pause = true;
        IPAddress _ip = IPAddress.None;
        byte[] _buffer = new byte[1];
        Socket _socket = null;
        IPEndPoint _endpoint = null;
        int[] _acktracker = new int[100];
        Mutex _ackmutex = new Mutex(false);
        Thread _t = null;
        bool _abort = false;

        public Form1()
        {
            ResetTracker();
            InitializeComponent();
        }

        int ReceivedPct()
        {
            _ackmutex.WaitOne();

            int receivedPct = _acktracker.Length;

            for (int i = 0; i < _acktracker.Length; i++)
            {
                if (_acktracker[i] >= 20 && _acktracker[i] <= _sequence && _acktracker[i] > _sequence - 100)
                {
                    receivedPct--;
                }
            }

            _ackmutex.ReleaseMutex();

            return (int)((long)receivedPct * 100 / (long)_acktracker.Length);
        }

        void ResetTracker()
        {
            _ackmutex.WaitOne();

            for (int i = 0; i < _acktracker.Length; i++)
            {
                _acktracker[i] = -1;
            }

            _ackmutex.ReleaseMutex();
        }

        void RecordSend(int seq)
        {
            _ackmutex.WaitOne();

            for (int i = 0; i < _acktracker.Length; i++)
            {
                if (_acktracker[i] < 0 || _acktracker[i] < seq - 100)
                {
                    _acktracker[i] = seq;
                    break;
                }
            }

            _ackmutex.ReleaseMutex();
        }

        void RecordReceipt(int seq)
        {
            _ackmutex.WaitOne();

            for (int i = 0; i < _acktracker.Length; i++)
            {
                if (_acktracker[i] == seq)
                {
                    _acktracker[i] = -1;
                    break;
                }
            }

            _ackmutex.ReleaseMutex();
        }

        void Monitor()
        {
            IPEndPoint ep = new IPEndPoint(IPAddress.Any, 22222);
            //UdpClient newsock = new UdpClient(ep);
            UdpClient newsock = new UdpClient();
            newsock.ExclusiveAddressUse = false;
            newsock.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            newsock.Client.Bind(ep);

            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            while (!_abort)
            {
                if (newsock.Available > 0)
                {
                    byte[] message = newsock.Receive(ref sender);

                    // only accept ack from the ip we are sending to
                    if (sender.Address.ToString() == _ip.ToString())
                    {
                        if (message[0] == 0xBC)
                        {
                            int receivedseq = (((int)message[2]) << 8) + message[1];
                            RecordReceipt(receivedseq);
                            SetReceivedPct(ReceivedPct());
                        }
                        else
                        {
                            // ignore this as it is not an ack packet
                        }
                    }
                }
                else
                {
                    Thread.Sleep(timer1.Interval / 2);
                }
            }
        }

        delegate void SetReceivedPctCallback(int receivedpct);

        private void SetReceivedPct(int receivedpct)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.

            if (label7.InvokeRequired)
            {
                SetReceivedPctCallback d = new SetReceivedPctCallback(SetReceivedPct);
                try
                {
                    Invoke(d, new object[] { receivedpct });
                }
                catch
                {
                }
            }
            else
            {
                label7.Text = "Last 100: " + receivedpct.ToString() + "%";

                // change text colour back and forward to show ACK packets are being received.
                if (label7.ForeColor == Color.Black)
                {
                    label7.ForeColor = Color.Red;
                }
                else
                {
                    label7.ForeColor = Color.Black;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _pause = !_pause;

            if (!_pause)
            {
                _sequence = 19;
                ResetTracker();
                timer1.Interval = (int)numericUpDown2.Value;
                timer1.Enabled = true;
            }
            else
            {
                timer1.Enabled = false;
            }
            ValidateWindow();
        }

        void ValidateWindow()
        {
            if (_ip == IPAddress.None)
            {
                _pause = true;
                timer1.Enabled = false;
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }

            if (_pause)
            {
                timer1.Enabled = false;
                button1.Text = "Start";
            }
            else
            {
                button1.Text = "Pause";
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _ip = IPAddress.Parse(textBox1.Text);
            }
            catch
            {
                _ip = IPAddress.None;
            }
            ValidateWindow();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ThreadStart ts = new ThreadStart(Monitor);
            _t = new Thread(ts);
            _t.Start();

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //_socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            //_socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, false);
            _socket.Ttl = 1;

            try
            {
                _ip = IPAddress.Parse(textBox1.Text);
            }
            catch
            {
                _ip = IPAddress.None;
            }
            ValidateWindow();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_pause)
            {
                timer1.Enabled = false;
            }
            else
            {
                if (_buffer.Length != numericUpDown1.Value)
                {
                    _buffer = new byte[(int)numericUpDown1.Value];
                    for (int i = 0; i < numericUpDown1.Value; i++)
                    {
                        _buffer[i] = 0xAB;
                    }
                }

                if (_endpoint == null || _ip != _endpoint.Address || numericUpDown3.Value != _endpoint.Port)
                {
                    _endpoint = new IPEndPoint(_ip, (int)numericUpDown3.Value);
                    _sequence = 19;
                }

                if (timer1.Interval != (int)numericUpDown2.Value)
                {
                    timer1.Interval = (int)numericUpDown2.Value;
                }

                _buffer[0] = (byte)((_sequence & 0xFF00) >> 8);
                _buffer[1] = (byte)(_sequence & 0x00FF);
                label5.Text = _sequence.ToString();
                RecordSend(_sequence);
                _sequence++;

                if (_sequence >= 32000)
                {
                    _sequence = 19;
                    ResetTracker();
                }

                // send packet
                _socket.SendTo(_buffer, _endpoint);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _abort = true;
            timer1.Enabled = false;
            _t.Abort();
            _t.Join();
        }
    }
}
