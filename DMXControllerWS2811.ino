#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif
#include <avr/pgmspace.h>

#include "global.h"
#ifdef USEENC28J60
	#include <UIPEthernet.h>
	#include <UIPUdp.h>
#else
    #include <SPI.h>
	#include <Ethernet.h>
	#include <EthernetUdp.h>
#endif
#include "display.h"
#include "MyEthernet.h"

byte debug1 = 0;
byte debug2 = 0;
byte ethtest = 0;
byte debugudp = DEBUGUDP;
int maxpacketsbetweendisplay = MAXPACKETSBETWEENDISPLAY;
int maxloopsbetweenbulbupdates = MAXLOOPSBETWEENBULBUPDATES;
 
Display* _pDisplay = NULL;
MyEthernet* myethernet = NULL;

void setup()
{
	Utility::DebugInit();
	
	#ifndef USEENC28J60
		// disable the SD card by switching pin 4 high
		// not using the SD card in this program, but if an SD card is left in the socket,
		// it may cause a problem with accessing the Ethernet chip, unless disabled
		pinMode(4, OUTPUT);
		digitalWrite(4, HIGH);

		// Stop SS from floating
		pinMode(SS, OUTPUT);
		digitalWrite(SS, HIGH);
	#endif
		
	// activate pull ups
	pinMode(DEBUGBUTTON1, INPUT_PULLUP);
	pinMode(DEBUGBUTTON2, INPUT_PULLUP);
	#ifdef DEBUGDUMPPIN
		pinMode(DEBUGDUMPPIN, INPUT_PULLUP);
		Serial.print(F("Pull down pin "));
		Serial.print(DEBUGDUMPPIN);
		Serial.println(F(" to display dump data."));
		Serial.print(F("Current state debug dump (1 will dump): "));
		Serial.println(!digitalRead(DEBUGDUMPPIN));
	#endif

	pinMode(PROGRESSPIN, OUTPUT);
	pinMode(ERRORPIN, OUTPUT);
	
	// remember the debug button states
	debug1 = !digitalRead(DEBUGBUTTON1);
	debug2 = !digitalRead(DEBUGBUTTON2);

	#ifdef DEBUGSERIAL
		Serial.print(F("Debug button 1 "));
		Serial.println(debug1);
		Serial.print(F("Debug button 2 "));
		Serial.println(debug2);
	#endif

	// sleep for a while to ensure the W5200 has time to start
	for (int i = 0; i < 5; i++)
	{
		digitalWrite(PROGRESSPIN, 1);
		digitalWrite(ERRORPIN, 1);
		delay(100);
		digitalWrite(PROGRESSPIN, 0);
		digitalWrite(ERRORPIN, 0);
		delay(100);
	}	
	
	myethernet = new MyEthernet();
	
	#ifdef DEBUGSERIAL
		Serial.println(F("Creating display"));
		Serial.print(F("F"));
		Serial.println(Utility::FreeRam());
	#endif
	
	_pDisplay = new Display();
	
	Utility::Debugln(F("Setup done"));
	Utility::Debug(F("F"));
	Utility::Debugln(Utility::FreeRam());
}

void loop()
{
	Utility::Debugln(F("Loop"));

	if (debugudp > 0)
	{
		Utility::DebugInit();
	}
	
	if (ethtest > 0)
	{
		_pDisplay->RunEthernetTest(ethtest);
	}
	else if (debug1 > 0 && debug2 == 0)
	{
		_pDisplay->RunDebug(1);
	}
	else if (debug1 == 0 && debug2 > 0)
	{
		_pDisplay->RunDebug(2);
	}
	else if ( debug1 > 0 && debug2 > 0)
	{
		_pDisplay->RunDebug(3);
	}
	else
	{
		Utility::Debugln(F("DMX Controller Mode."));

		// normal execution
		_pDisplay->Process();
	}
}