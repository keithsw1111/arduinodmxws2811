#ifndef SOCKETDEBUG_H
	#define SOCKETDEBUG_H

	#ifdef USEENC28J60
		#define SOCKET uint8_t
	#else
		#include <utility/socket.h>
	#endif
	
	extern void socketDumpStateAll(byte level);
	extern void socketDumpState(SOCKET s, byte level);

#endif