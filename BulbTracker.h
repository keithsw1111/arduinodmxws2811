#ifndef BULBTRACKER_H
	#define BULBTRACKER_H

	#include "global.h"
	
	// only need this class if we have WS2811 bulbs
	#ifdef UNIVERSE

		#include "DMX.h"
		#include "WS2811Protocol.h"
		#include "FastBitManipulation.h"

		class BulbTracker
		{
			// format of this data will be bulb 1 across up to 8 strings RRRRRRRRGGGGGGGGBBBBBBBB then bulb 2 and so on.
			byte* _pBulbData;
			byte* _pSendBuffer;
			
			// _tracker helpers
			#ifdef TRACKCHANGES
			bool _tracker[MAXBULBS];
			inline void SetAllDirty() { memset(&_tracker[0], 0x01, MAXBULBS); };
			inline void SetAllClean() { memset(&_tracker[0], 0x00, MAXBULBS); };
			inline void SetClean(byte index) { _tracker[index] = false; };
			inline void SetDirty(byte index) { _tracker[index] = true; };
			inline bool IsDirty(byte index) { return _tracker[index]; };
			void DumpTracker();
			#endif
			
			void DumpBulbData();
			void DumpSendBuffer();

			public:
			BulbTracker();
			void SetAllColour(byte r, byte g, byte b);
			void SetStringColour(byte r, byte g, byte b, byte string);
			void SetBulbColour(byte r, byte g, byte b, byte string, short bulb);
			bool ProcessDMX(DMX* pDMX);
			void Display();
			void Dump();
		};
	#endif
#endif