#include "global.h"
#include "Display.h"
#include "myethernet.h"
//#include "timer.h"

Display::Display()
{
	#ifdef DETECTMISSINGSEQUENCENUMBERS
		for (byte i = 0; i < TOTALUNIVERSES; i++)
		{
			_lastSequence[i] = -1;
		}
	#endif

	#ifdef UNIVERSE
		// display black to start with
		Utility::Debugln(F("Startup: Initialise ... Black"));
		_bulbTracker.Display();
	#endif
	
	#ifdef STARTPATTERN
		Utility::Debugln(F("Before start pattern."));
		Utility::Debug(F("F"));
		Utility::Debugln(Utility::FreeRam());

		#ifdef RELAYUNIVERSE
			_relays.SetAllState(true);
		#endif
		#ifdef UNIVERSE
			Utility::Debugln(F("Startup: Red"));
			_bulbTracker.SetAllColour(255,0,0);
			_bulbTracker.Display();
		#endif
		delay(500);

		#ifdef RELAYUNIVERSE
			_relays.SetAllState(false);
		#endif
		#ifdef UNIVERSE
			Utility::Debugln(F("Startup: Yellow"));
			_bulbTracker.SetAllColour(255,255,0);
			_bulbTracker.Display();
		#endif
		delay(500);

		#ifdef RELAYUNIVERSE
			_relays.SetAllState(true);
		#endif
		#ifdef UNIVERSE
			Utility::Debugln(F("Startup: Green"));
			_bulbTracker.SetAllColour(0,255,0);
			_bulbTracker.Display();
		#endif
		delay(500);

		#ifdef RELAYUNIVERSE
			_relays.SetAllState(false);
		#endif
		#ifdef UNIVERSE
			Utility::Debugln(F("Startup: Cyan"));
			_bulbTracker.SetAllColour(0,255,255);
			_bulbTracker.Display();
		#endif
		delay(500);

		#ifdef RELAYUNIVERSE
			_relays.SetAllState(true);
		#endif
		#ifdef UNIVERSE
			Utility::Debugln(F("Startup: Blue"));
			_bulbTracker.SetAllColour(0,0,255);
			_bulbTracker.Display();
		#endif
		delay(500);
		
		#ifdef RELAYUNIVERSE
			_relays.SetAllState(false);
		#endif
		#ifdef UNIVERSE
			Utility::Debugln(F("Startup: Magenta"));
			_bulbTracker.SetAllColour(255,0,255);
			_bulbTracker.Display();
		#endif
		delay(500);
		
		#ifdef RELAYUNIVERSE
			_relays.SetAllState(true);
		#endif
		#ifdef UNIVERSE
			Utility::Debugln(F("Startup: White"));
			_bulbTracker.SetAllColour(128,128,128);
			_bulbTracker.Display();
		#endif
		delay(500);
		
		#ifdef RELAYUNIVERSE
			_relays.SetAllState(false);
		#endif
		#ifdef UNIVERSE
			Utility::Debugln(F("Startup: Black"));
			_bulbTracker.SetAllColour(0,0,0);
			_bulbTracker.Display();
		#endif
		
		Utility::Debugln(F("Start pattern done."));
		Utility::Debug(F("F"));
		Utility::Debugln(Utility::FreeRam());
	#else
		#ifdef RELAYUNIVERSE
			_relays.SetAllState(false);
		#endif
		#ifdef UNIVERSE
			Utility::Debugln(F("Initially Black"));
			_bulbTracker.SetAllColour(0,0,0);
			_bulbTracker.Display();
		#endif
	#endif
	
	myethernet->StartUDP();
}
		
void Display::RouteDMX()
{
	#ifdef DEBUGSERIAL
		Utility::Debug(F("Display::RouteDMX has packet to route. us:"));
		Utility::Debugln(micros());
	#endif

	#ifdef RELAYUNIVERSE
		// check relay first because it is faster
		if (RELAYUNIVERSE == _dmx.Universe())
		{
			//Timer processrelay("   processrelay");
			_relays.ProcessDMX(&_dmx);
			//processrelay.DumpSerial();
		}
		else
	#endif

	// no ... so assume it is a WS2811 bulb universe
	if (_dmx.Universe() == 22222)
	{
		short i1 = _dmx.ReadWord();
		short i2 = _dmx.ReadWord();

		Utility::Debug(F("22222: Control packet received. Type: "));
		Utility::Debug(i1);
		Utility::Debug(F(" Data: "));
		Utility::Debugln(i2);
		
		if (i1 == 1)
		{
			Utility::Debugln(F("22222: Switching to debug mode 1."));
			debug1 = 1;
			debug2 = 0;
			ethtest = 0;
		}
		else if (i1 == 2)
		{
			Utility::Debugln(F("22222: Switching to debug mode 2."));
			debug1 = 0;
			debug2 = 1;
			ethtest = 0;
		}
		else if (i1 == 3)
		{
			Utility::Debugln(F("22222: Switching to debug mode 3."));
			debug1 = 1;
			debug2 = 1;
			ethtest = 0;
		}
		else if (i1 == 0)
		{
			Utility::Debugln(F("22222: Enabling DMX mode."));
			debug1 = 0;
			debug2 = 0;
			ethtest = 0;
		}
		else if (i1 == 255)
		{
			// reset
			Utility::Debugln(F("22222: Resetting arduino."));
			Utility::Reset();
		}
		else if (i1 == 254)
		{
			if (i2 == 1)
			{
				Utility::Debugln(F("22222: Turning on UDP debugging."));
				debugudp = 1;
				myethernet->SetDebugIP(myethernet->RemoteIP());
			}
			else
			{
				Utility::Debugln(F("22222: Turning off UDP debugging."));
				debugudp = 0;
			}
		}
		else if (i1 == 253)
		{
			maxpacketsbetweendisplay = i2;
		}
		else if (i1 == 252)
		{
			maxloopsbetweenbulbupdates = i2;
		}
		else if (i1 == 250)
		{
			Utility::Debugln(F("22222: Enabling ethernet test mode."));
			myethernet->SetDebugIP(myethernet->RemoteIP());
			ethtest = i2;
			debug1 = 0;
			debug2 = 0;
		}
		else if (i1 == 249)
		{
			Utility::Debugln(F("22222: Resetting ethernet connection."));
			delete myethernet;
			myethernet = new MyEthernet();
			myethernet->StartUDP();
		}
		else if (i1 == 248)
		{
			Utility::Debugln(F("22222: Dumping ethernet settings."));
			myethernet->Dump();
		}
	}
	#ifdef UNIVERSE
	else if (_bulbTracker.ProcessDMX(&_dmx))
	{
		// WS2811 packet processed
	}
	#endif
	else
	{
		#ifdef DEBUGSERIAL
			Utility::Debug(F("Packet could not be routed to universe "));
			Utility::Debugln(_dmx.Universe());
		#endif
	}
}

void Display::Process()
{
	unsigned long timesincelastpacket = millis();
	bool defaulton = false;

	Utility::Debug(F("DMX: Allocated IP Address "));
	Utility::Debugln(myethernet->LocalIP());

	bool flashon = false;
	int loopssinceupdate = 0;
	int loopssincepacket = 0;
	
	//Timer betweenpackets("betweenpackets");
	while (true)
	{
		// clear all network packets
		// because this is so much slower than display best to clear everything so we keep preparing and processing until prepare returns false because there is no packet.
		byte rcv = 0;
		//Timer preparepacket("   preparepacket");
		//Timer preparepacketnone("   preparepacketnone");
		while (rcv < maxpacketsbetweendisplay && _dmx.PreparePacket(false, 0))
		{
			#ifdef DETECTMISSINGSEQUENCENUMBERS
				byte index = -1;
				#ifdef UNIVERSE
					index = UNIVERSE - _dmx.Universe();
				#endif
				#ifdef RELAYUNIVERSE
					if (_dmx.Universe() == RELAYUNIVERSE)
					{
						index = TOTALUNIVERSES - 1;
					}
				#endif
				
				if (_lastSequence[index] == -1)
				{
					// ignore this as last sequence is not correct
				}
				#ifdef USEDDP
				else if (_lastSequence[index] == 15)
				#else
				else if (_lastSequence[index] == 255)
				#endif
				{
					if (_dmx.Sequence() != 0)
					{
						Utility::Debug(F("For universe "));
						Utility::Debug(_dmx.Universe());
						Utility::Debug(F(" sequence # "));
						Utility::Debug(_dmx.Sequence());
						Utility::Debug(F(" should not follow sequence # "));
						Utility::Debugln(_lastSequence[index]);
					}
				}
				else
				{
					if (_dmx.Sequence() != _lastSequence[index] + 1)
					{
						Utility::Debug(F("WARNING: For universe "));
						Utility::Debug(_dmx.Universe());
						Utility::Debug(F(" sequence # "));
						Utility::Debug(_dmx.Sequence());
						Utility::Debug(F(" should not follow sequence # "));
						Utility::Debugln(_lastSequence[index]);
					}
				}
				_lastSequence[index] = _dmx.Sequence();
			#endif
			
			timesincelastpacket = millis();
			if (defaulton)
			{
				defaulton = false;
				#ifdef RELAYUNIVERSE
					_relays.SetAllState(false);
				#endif
				#ifdef UNIVERSE
					_bulbTracker.SetAllColour(0,0,0);
					_bulbTracker.Display();
				#endif
			}
			
			Utility::Debug(F("DMX: Loops since last packet found "));
			Utility::Debugln(loopssincepacket);
			loopssincepacket = 0;

			//preparepacket.DumpSerial();
			//betweenpackets.DumpSerial();
			//betweenpackets.Reset();
			
			//Timer routeDMX("      routeDMX");
			RouteDMX();
			//routeDMX.DumpSerial();
			_dmx.Flush();
			rcv++;
		}

		loopssinceupdate++;
		loopssincepacket++;
		
		if (rcv > 0 || (maxloopsbetweenbulbupdates != -1 && loopssinceupdate > maxloopsbetweenbulbupdates) && millis() - timesincelastpacket < DEFAULTONTIME * 1000)
		{
			#ifdef DEBUGSERIAL
				Utility::Debug(F("DMX: About to do display: rcv = "));
				Utility::Debug(rcv);
				Utility::Debug(F(", loopssinceupdate = "));
				Utility::Debugln(loopssinceupdate);
			#endif
			
			flashon = !flashon;
			if (flashon)
			{
				digitalWrite(PROGRESSPIN, 1);
			}
			else
			{
				digitalWrite(PROGRESSPIN, 0);
			}
			
			#ifdef UNIVERSE
				// now display bulbs
				_bulbTracker.Display();
			#endif
			loopssinceupdate = 0;
			
			#ifdef DEBUGDUMPPIN
				#ifdef UNIVERSE
					if (!digitalRead(DEBUGDUMPPIN))
					{
						// pin pulled low so lets dump our state
						Utility::Debugln(F("Bulb Data "));
						_bulbTracker.Dump();
					}
				#endif
			#endif
		}
		else if (millis() - timesincelastpacket > (unsigned long)DEFAULTONTIME * 1000)
		{
			#ifdef REBOOTONPACKETTIMEOUT
				Utility::Debugln(F("Packet not seen for a long time ... rebooting."));
				Utility::Reset();
			#else
				Utility::Debugln(F("Packet not seen for a long time ... default turned on ... and resetting network."));
				defaulton = true;
				
				#ifdef RELAYUNIVERSE
					if (DEFAULTRELAYSTATE == ON)
					{
					_relays.SetAllState(true);
					}
					else
					{
					_relays.SetAllState(false);
					}
				#endif
				#ifdef UNIVERSE
					_bulbTracker.SetAllColour(DEFAULTCOLOUR);
					_bulbTracker.Display();
				#endif

				timesincelastpacket = millis();

				// reset the network card
				delete myethernet;
				myethernet = new MyEthernet();
				myethernet->StartUDP();
			#endif
		}
		
		//Timer checkinterupt("      checkinterrupt");
		byte debugt = !digitalRead(DEBUGBUTTON1);
		if (debugt > 0)
		{
			Utility::Debugln(F("DMX: Entering suspend state."));
			#ifdef RELAYUNIVERSE
				if (DEFAULTRELAYSTATE == ON)
				{
					_relays.SetAllState(true);
				}
				else
				{
					_relays.SetAllState(false);
				}
			#endif
			while(true)
			{
				if (CheckStateChange(false, 0))
				{
					return;
				}
			}
		}
		
		// if debug flags are on then we need to return to enter new state
		if (debug1 > 0 || debug2 > 0 || ethtest > 0)
		{
			return;
		}
		//checkinterupt.DumpSerial();	
	}
}

bool Display::CheckStateChange(bool skip2, int packetSize)
{
	bool s2 = skip2;
	int ps = packetSize;
	
	// clear all the backed up packets
	while (_dmx.PreparePacket(s2, ps))
	{
		// only use default values the first time
		s2 = false;
		ps = 0;
	
		if (_dmx.Universe() == 22222)
		{
			byte olddebug1 = debug1;
			byte olddebug2 = debug2;
			byte oldethtest = ethtest;
			RouteDMX();
			_dmx.Flush();

			if (debug1 != olddebug1 || debug2 != olddebug2 || ethtest != oldethtest)
			{
				return true;
			}
		}
	}
	
	return false;
}

void Display::RunEthernetTest(byte testtype)
{
	// test type 1 - verbosely display everything
	// test type 2 - display only when it is missing
	// test type 3 - display only when missing + send acknowledgement packet

	Utility::Debug(F("ET: Entering Ethernet Test Mode: "));
	Utility::Debugln(testtype);
	
	// sequences start at 20 to avoid clash with potential real DMX packet
	short lastsequence = 19;

	while(true)
	{
		int packetSize = myethernet->ParsePacket();
		#ifdef DEBUGSERIAL
			//Utility::Debug(F("Looking for an ET Packet ... found bytes "));
			//Utility::Debugln(packetSize);
		#endif

		if (packetSize >= 2)
		{
			#ifdef DEBUGSERIAL
				//myethernet->Dump(0);
				Utility::Debugln(F("ET: Packet received ..."));
			#endif

			short s;
			byte b1;
			byte b2;
			 myethernet->Read((unsigned char*)&b1, sizeof(b1));
			 myethernet->Read((unsigned char*)&b2, sizeof(b2));
			s = (((short)b1) << 8) + b2;

			// is it likely this is a real DMX packet
			if (s == 0x0010 && packetSize > 125)
			{
				#ifdef DEBUGSERIAL
					Utility::Debugln(F("ET: Packet looks like it might be an ARTNET packet."));
				#endif
				
				// probably not a test packet
				if (CheckStateChange(true, packetSize))
				{
					Utility::Debugln(F("ET: Exit Ethernet Test Mode."));
					return;
				}
			}
			else
			{
				// check it is not a loop around back to 0 packet
				if (s != 19)
				{
					if (testtype == 1 || s != lastsequence + 1)
					{
						Utility::Debug(F("ET: Packet received. Size: "));
						Utility::Debug(packetSize);
						Utility::Debug(F(" Sequence: "));
						Utility::Debug(s);
					}

					if (s == lastsequence + 1)
					{
						if (testtype == 1)
						{
							Utility::Debugln(F(""));
						}
					}
					else
					{
						Utility::Debug(F(" Missed packets: "));
						Utility::Debugln(s - lastsequence - 1);
					}
				}

				if (testtype == 3)
				{
					// we need to acknowledge the packet
					byte resp[3];
					resp[0] = 0xBC;
					memcpy(&resp[1], &s, sizeof(s));
					#ifdef DEBUGSERIAL
						//Utility::Debug(F("Acknowledging packet "));
						//Utility::Debug(s);
						//Utility::Debug(F(" "));
						//Utility::Debug(resp[0], HEX);
						//Utility::Debug(F(" "));
						//Utility::Debug(resp[1], HEX);
						//Utility::Debug(F(" "));
						//Utility::Debugln(resp[2], HEX);
					#endif
					myethernet->SendAckPacket(&resp[0], sizeof(resp));
				}
				
				lastsequence = s;
				
				// really read the data because flush hides the processing time
				for (int i = 2; i < packetSize; i++)
				{
					byte b;
					myethernet->Read((unsigned char*)&b, sizeof(b));
				}
			}
			_dmx.Flush();
		}
		else if (packetSize < 0)
		{
			#ifdef DEBUGSERIAL
				Utility::Debug(F("ET: Negative sized packet ... not a good sign: "));
				Utility::Debugln(packetSize);
			#endif
		}
		else if (packetSize == 1)
		{
			_dmx.Flush();
			#ifdef DEBUGSERIAL
				Utility::Debug(F("ET: Odd sized packet: "));
				Utility::Debugln(packetSize);
			#endif
		}
	}
}

void Display::RunDebug(byte debug)
{
	if (debug == 1)
	{
		Utility::Debugln(F("D1: Debug Mode 1 - Primary Colour Cycling."));

		// debug mode 1
		// cycle all primary colours
		while(true)
		{
			digitalWrite(PROGRESSPIN, 1);
			delay(100);
			digitalWrite(PROGRESSPIN, 0);

			Utility::Debugln(F("D1: Debug Mode 1 - Red."));

			#ifdef RELAYUNIVERSE
				_relays.SetAllState(true);
			#endif
			#ifdef UNIVERSE
				_bulbTracker.SetAllColour(255, 0, 0);
				_bulbTracker.Display();
			#endif
			
			if (CheckStateChange(false, 0))
			{
				return;
			}
			#ifdef DEBUGDUMPPIN
				#ifdef UNIVERSE
					if (!digitalRead(DEBUGDUMPPIN))
					{
						// pin pulled low so lets dump our state
						Utility::Debugln(F("Bulb Data "));
						_bulbTracker.Dump();
					}
				#endif
			#endif
			delay(500);

			Utility::Debugln(F("D1: Debug Mode 1 - Green."));

			#ifdef RELAYUNIVERSE
				_relays.SetAllState(false);
			#endif
			#ifdef UNIVERSE
				_bulbTracker.SetAllColour(0, 255, 0);
				_bulbTracker.Display();
			#endif

			if (CheckStateChange(false, 0))
			{
				return;
			}
			#ifdef DEBUGDUMPPIN
				#ifdef UNIVERSE
					if (!digitalRead(DEBUGDUMPPIN))
					{
						// pin pulled low so lets dump our state
						Utility::Debugln(F("Bulb Data "));
						_bulbTracker.Dump();
					}
				#endif
			#endif
			delay(500);

			Utility::Debugln(F("D1: Debug Mode 1 - Blue."));

			#ifdef UNIVERSE
				_bulbTracker.SetAllColour(0, 0, 255);
				_bulbTracker.Display();
			#endif
			if (CheckStateChange(false, 0))
			{
				return;
			}
			#ifdef DEBUGDUMPPIN
				#ifdef UNIVERSE
					if (!digitalRead(DEBUGDUMPPIN))
					{
						// pin pulled low so lets dump our state
						Utility::Debugln(F("Bulb Data "));
						_bulbTracker.Dump();
					}
				#endif
			#endif
			delay(500);
		}
	}
	else if (debug == 2)
	{
		Utility::Debugln(F("D2: Debug Mode 2 - One bulb on each string 0->MAXBULBS."));

		// debug mode 2
		// display one bulb at a time starting at the start
		int current  = 0;
		while(true)
		{
			digitalWrite(PROGRESSPIN, 1);
			delay(100);
			digitalWrite(PROGRESSPIN, 0);
			
			#ifdef RELAYUNIVERSE
				if (current <= _relays.RelayCount())
				{
					if (current == 0)
					{
						_relays.SetOneRelay(_relays.RelayCount() -1 , 0);
						_relays.SetOneRelay(current, 1);
					}
					else if (current == _relays.RelayCount())
					{
						_relays.SetOneRelay(current-1, 0);
					}
					else
					{
						_relays.SetOneRelay(current-1, 0);
						_relays.SetOneRelay(current, 1);
					}
				}
			#endif
			#ifdef UNIVERSE
				for (byte j=0; j < OUTPUTSTRINGS; j++)
				{
					if (current == 0)
					{
						_bulbTracker.SetBulbColour(0,0,0, j, MAXBULBS-1);
						_bulbTracker.SetBulbColour(128,128,128, j, current);
					}
					else if (current == MAXBULBS)
					{
						_bulbTracker.SetBulbColour(0,0,0, j, MAXBULBS-1);
					}
					else
					{
						_bulbTracker.SetBulbColour(0,0,0, j, current-1);
						_bulbTracker.SetBulbColour(128,128,128, j, current);
					}
				}
				_bulbTracker.Display();
			#endif

			current ++;
			#ifdef UNIVERSE
				if (current >= MAXBULBS)
				{
					current = 0;
				}
			#else
				if (current >= _relays.RelayCount())
				{
					current = 0;
				}
			#endif

			if (CheckStateChange(false, 0))
			{
				return;
			}
				
			#ifdef DEBUGDUMPPIN
				#ifdef UNIVERSE
					if (!digitalRead(DEBUGDUMPPIN))
					{
						// pin pulled low so lets dump our state
						Utility::Debugln(F("Bulb Data "));
						_bulbTracker.Dump();
					}
				#endif
			#endif
			
			delay(100);
		}
	}
	else if (debug == 3)
	{
		Utility::Debugln(F("D3: Debug Mode 3 - Show string number."));

		//debug mode 3
		// display the number of bulbs according to which string number
		while(true)
		{
			digitalWrite(PROGRESSPIN, 1);
			delay(100);
			digitalWrite(PROGRESSPIN, 0);

			Utility::Debugln(F("D3: Debug Mode 3 - Red."));

			#ifdef UNIVERSE
				for (byte i =0; i < OUTPUTSTRINGS; i++)
				{
					for (byte j = 0; j < i + 1; j++)
					{
						#ifdef DEBUGSERIAL
							//Utility::Debug(F("Setting red string "));
							//Utility::Debug(i);
							//Utility::Debug(F(" bulb "));
							//Utility::Debugln(j);
						#endif
						_bulbTracker.SetBulbColour(255,0,0, i, j);
					}
				}
				_bulbTracker.Display();
			#endif

			if (CheckStateChange(false, 0))
			{
				return;
			}

			#ifdef DEBUGDUMPPIN
				#ifdef UNIVERSE
					if (!digitalRead(DEBUGDUMPPIN))
					{
						// pin pulled low so lets dump our state
						Utility::Debugln(F("Bulb Data "));
						_bulbTracker.Dump();
					}
				#endif
			#endif
			delay(500);

			Utility::Debugln(F("D3: Debug Mode 3 - Green."));

			#ifdef UNIVERSE
				for (byte i =0; i < OUTPUTSTRINGS; i++)
				{
					for (byte j = 0; j < i + 1; j++)
					{
						_bulbTracker.SetBulbColour(0,255,0, i, j);
					}
				}
				_bulbTracker.Display();
			#endif
			
			if (CheckStateChange(false, 0))
			{
				return;
			}
			
			#ifdef DEBUGDUMPPIN
				#ifdef UNIVERSE
					if (!digitalRead(DEBUGDUMPPIN))
					{
						// pin pulled low so lets dump our state
						Utility::Debugln(F("Bulb Data "));
						_bulbTracker.Dump();
					}
				#endif
			#endif
			delay(500);

			Utility::Debugln(F("D3: Debug Mode 3 - Blue."));

			#ifdef UNIVERSE
				for (byte i =0; i < OUTPUTSTRINGS; i++)
				{
					for (byte j = 0; j < i + 1; j++)
					{
						_bulbTracker.SetBulbColour(0,0,255, i, j);
					}
				}
				_bulbTracker.Display();
			#endif
			
			if (CheckStateChange(false, 0))
			{
				return;
			}
			
			#ifdef DEBUGDUMPPIN
				#ifdef UNIVERSE
					if (!digitalRead(DEBUGDUMPPIN))
					{
						// pin pulled low so lets dump our state
						Utility::Debugln(F("Bulb Data "));
						_bulbTracker.Dump();
					}
				#endif
			#endif

			#ifdef RELAYUNIVERSE
				for (byte i =0; i < _relays.RelayCount(); i++)
				{
					for (byte j = i; j < _relays.RelayCount(); j++)
					{
						_relays.SetOneRelay(j, 1);
					}
					delay(200);

					for (byte j = 0; j <= i; j++)
					{
						_relays.SetOneRelay(j, 0);
					}
					delay(200);

					if (CheckStateChange(false, 0))
					{
						return;
					}
				}
			#endif
		}
	}
}