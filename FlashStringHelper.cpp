#include "FlashStringHelper.h"
#include <avr/pgmspace.h>

// calculate the length of a string in Flash RAM
short FlashStringHelper::Length(const __FlashStringHelper* ps)
{
	short len = 0;

	// start at the beginning
	const char PROGMEM* p = (const char PROGMEM*)ps;

	// while we have not hit the null terminator
	while(pgm_read_byte(p++) != 0)
	{
		// add one to the length
		len++;
	}

	return len;
}

// load a string from flash
FlashStringHelper::FlashStringHelper(const __FlashStringHelper* ps)
{
	// need the length to start with
	short len = FlashStringHelper::Length(ps);

	// allocate memory for the string
	buffer = (char*)malloc(len + 1);

	if (buffer == NULL)
	{
		return;
	}

	// clear the memory
	memset(buffer, 0x00, len + 1);

	// start at the beginning of the string
	const char PROGMEM* p = (const char PROGMEM*)ps;
	short i = 0;

	// copy the string
	while(pgm_read_byte(p) != 0)
	{
		*(buffer+i) = pgm_read_byte(p);
		p++;
		i++;
	}
}

// Destructor
FlashStringHelper::~FlashStringHelper()
{
	// free the memory
	free(buffer);
	buffer = NULL;
}

// Get the string as a char array
char* FlashStringHelper::GetString()
{
	return buffer;
}
