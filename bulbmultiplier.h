#ifndef BULBMULTIPLIER_H
	#define BULBMULTIPLIER_H

	#ifdef BULBMULTIPLIER
	
	// source channel (first of 3 ... RGB), start output bulb, end output bulb
	short BulbMultiplier[][3] PROGMEM = {
									{0, 0, 0}, // terminator
					           };
					  
	#endif
	
#endif